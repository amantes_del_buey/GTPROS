analisis.pdf: documento_de_analisis.md
	pandoc -o $@ --template=main.tex $<

clean:
	rm analisis.pdf

.PHONY: clean
