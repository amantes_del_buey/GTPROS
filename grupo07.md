Entrega de práctica del Grupo 07
================================

Planificación y Gestión de Proyectos.

Escuela de Ingeniería Informática de Valladolid, Universidad de Valladolid.

13 de enero de 2015


Enlaces
-------
La aplicación desarrollada se encuentra desplegada en la url [http://virtual.lab.inf.uva.es:27072/](http://virtual.lab.inf.uva.es:27072/). Desde allí se enlaza al resto de documentos requeridos.

Datos de acceso
---------------

### Administrador de la aplicación

- Nombre de usuario: `admin`
- Contraseña: `1234`

### Usuario normal
Es jefe de proyecto en algunos proyectos y en otros cumple otros roles.

- Nombre de usuario: `user`
- Contraseña: `1234`

Hemos añadido otros usuarios normales para comprobar rápidamente las interacciones entre éstos.

- Nombre de usuario: `jorcuad`
- Contraseña: `1234`

- Nombre de usuario: `samalfa`
- Contraseña: `1234`

- Nombre de usuario: `adrdela`
- Contraseña: `1234`
