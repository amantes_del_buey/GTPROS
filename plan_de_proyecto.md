% Proyecto GTPROS: Gestión de Tiempo para PROyectos de Software
% Adrián de la Rosa Martín; Jorge Cuadrado Saez; Samuel Alfageme Sanz
% 4 de noviembre de 2015

# Visión general
## Resumen del proyecto
Buscamos desarrollar una aplicación que facilite a los gestores de proyectos de software realizar el seguimiento temporal del trabajo que realizan, durante el desarrollo de un proyecto, todos los trabajadores implicados en las diferentes actividades que se realizan en dicho proceso.

### Propósito, alcance y objetivos
El objetivo final de este proyecto es el desarrollo de la herramienta anteriormente descrita.

Un objetivo secundario del desarrollo de una herramienta de este tipo es que los gestores de proyectos utilicen los datos temporales de los proyectos finalizados para ayudar a planificar y presupuestar nuevos proyectos y para analizar las prácticas de trabajo seguidas con el fin de servir de ayuda en cualquier proceso de mejora del proceso de desarrollo de software.

#### Requisitos
Los requisitos mínimos que permiten a la aplicación cumplir sus objetivos son:

- La plataforma permitirá monitorizar el coste y el esfuerzo de cada proyecto.
- La plataforma devolverá dos datos una vez acabado el proyecto, el esfuero estimado y por otro lado el esfuerzo real.
- La plataforma considerará tres tipos de usuario: Administrador, Jefe de proyecto, desarrollador.
- La interfaz con el sistema será un navegador web, siendo posible usar la paltaforma desde cualquier navegador.

### Supuestos y restricciones
Tendremos en cuenta que el resultado del proyecto se valorará en base a los siguientes criterios:

- La consecución de los objetivos del proyecto.
- Los datos sobre seguimiento y control del proyecto.
- La valoración de la solución técnica implantada y la documentación presentada.
- Las respuestas a las preguntas que se realicen durante el acto de revisión.

También tenemos en cuenta las siguientes restricciones:

- La persistencia de los datos de la aplicación se realizará en la versión, instalación e instancia de MySQL que proveerá el cliente.
- Se respetarán las fechas de entrega marcadas para los hitos en la especificación de la práctica.
- El cliente exige que se le entreguen unos documentos antes, durante y después del desarrollo del proyecto.
- Puesto que la interfaz con el sistema debe ser a través de un navegador web, tenemos restringidas las tecnologías que podemos usar para ello.

### Entregables del proyecto
En el primer hito, deberemos generar un plan de desarrollo del proyecto, sigiendo una plantilla estándar. Dicho documento se trata de éste mismo. En el segundo hito, entregaremos el documento de análisis y diseño de la aplicación.

El proyecto deberá generar a su término una herramienta con todas las funciones especificadas operativas y todos los soportes para la distribución de la aplicación. Se entiende que el código estará suficientemente comentado para su correcta comprensión.

También, se deberán entregar los siguientes documentos:

- Documento de análisis y diseño. Partimos de una elicitación de requisitos para definir las user stories que debemos realizar con nuestro producto. Diseñaremos usando modelos UML la implementación que realizaremos después.
- Documento de implementación. En él se describirá el método de distribución del software a desarrollar, indicando el entorno necesario, la estructura de directorios que organizan el código fuente, y las instrucciones para compilar el código y obtener un ejecutable. También se describirán las tareas desarrolladas en cada sprint, indicando a qué versión del sistema de control de versiones corresponde el final de cada sprint.
- Documento de seguimiento del proyecto, incluyendo las medidas tomadas durante el desarrollo del mismo.
- Manual de usuario y manual de instalación de la aplicación generada.

### Programación temporal maestra y resumen de presupuesto
La programación temporal gira alrededor de tres hitos impuestos por el cliente:

- Hito 1 (5 de Noviembre de 2015): Se deberá entregar un plan de proyecto ajustado al modelo de proceso elegido, recomendando seguir alguna plantilla estándar.
- Hito 2 (9 de Diciembre de 2015): Se deberá entregar el documento de análisis de la aplicación.
- Hito 3 (13 de Enero de 2016): Entrega de la aplicación con todas sus funciones operativas y los soportes necesarios para la distribución de la aplicación. Se entregará también el documento de análisis y diseño, el documento de implementación, el documento de seguimiento del proyecto y el manual de usuario y de instalación.

Definimos a mayores un número de hitos para ayudarnos con el seguimiento del proyecto:

- Hito 1.1(19 de Noviembre de 2015): Resultado del Sprint 1
- Hito 1.2(3 de Diciembre de 2015): Resultado del Sprint 2
- Hito 2.1(17 de Diciembre de 2015): Resultado del Sprint 3

## Evolución del plan
Se realizará un seguimiento durante el proyecto de las estimaciones realizadas sobre las tareas. Esto nos permitirá comprobar que se están alcanzando los objetivos de la forma esperada. En caso de que cualquier circunstancia obligue a un cambio dentro de la planificación se realizará una revisión de las consecuencias que esta alteración pueda tener, reestructurando el plan y buscando minimizar sus efectos. Toda modificación sobre este plan del proyecto será comunicada inmediatamente al cliente.

Gracias al carácter ágil del proyecto en la planificación, se irán mejorando progresivamente las estimaciones y corrigiendo defectos organizativos, gracias a la experiencia obtenida durante la consecución de los objetivos, mediante backlog groomings.

Cualquier modificación sobre este plan del proyecto será comunicada inmediatamente al cliente, por ser el principal interesado.

## Estructura del documento

# Referencias
## Estándares y documentos
Este documento está adaptado de los estándares de la IEEE para los planes de gestión de proyectos de software, 1058-1998, que se ajusta a los requisitos de el estándar ISO 12207 de procesos de ciclo de vida de software.

# Definiciones
A través de estos documentos usaremos algunos términos, en su mayoría extraídos de las metodología Scrum que usamos para organizar el trabajo, que pueden ser desconocidos si no se conoce este tipo de metodologías. Para que sirva de referencia en la lectura de este documento, definimos aquí esos términos:

- Backlog: Lista con todas las tareas que hay que hacer en algún momento pero sobre las que no se está trabajando en este momento.
- Feature: Característica.
- User story: Requisitos de alto nivel que definen situaciones de uso de los distintos tipos de usuario.
- Sprint: Es un periodo corto de tiempo en el que el trabajo designado para tal periodo debe realizarse y entregarse.
- Standup: Reunión corta que se realiza con todos los miembros del equipo y con una frecuencia diaria si es posible para que todos conozcan el estado del proyecto, lo tengan en cuenta y se eviten bloqueos.
- Acceptance criteria: Criterios que debe cumplir una feature o una user story para que sean consideradas como terminada.
- Scrum master: Es la persona encargada de mantener útiles y funcionales las herramientas de seguimiento del proyecto y de coordinar a las personas involucradas en él de forma que se cumplan los sprints decididos.
- Product owner: Nexo con el cliente que decide qué user stories son relevantes y qué features deben implementarse.
- Storypoints: Son puntos que se asignan a las tareas para estimar su duración. La equivalencia en tiempo de cada punto debe ser decidida por el equipo según las características del proyecto.
- Estimación fibonacci: Es un sistema de estimación en el que se asignan storypoints a las tareas en cantidades que crecen según la secuencia de fibonacci.
- Velocity: Ritmo al que se realizan las tareas en función de los storypoints que tienen asignadas. Permite corregir las estimaciones y valorar el ritmo de trabajo del equipo.
- Burndown chart: Gráfica que muestra la duración de las tareas restantes a lo largo del tiempo en comparación con el ritmo de completitud
- Epic: Un epic agrupa un conjunto de user stories relacionadas.
- Code review: Revisión entre pares que se realiza al código antes de que éste sea mezclado con la rama principal del repositorio de código compartido. Puede generar comentarios y propuestas de mejora que provoquen un ciclo de mejora de esa porción del código antes de que entre al conjunto de código común, aumentando la calidad de éste.
- Demo: Prueba que se hace del producto junto con el cliente de forma que éste pueda comprobar el estado del producto con frecuencia de forma que se permite modificar la dirección del producto más pronto, cuando es más sencillo realizar cambios.
- Reporter: Aquella persona que informa de un bug o subtarea.
- Merge request: En el ámbito del control de versiones, petición de integración de una rama de develop con la rama master del proyecto.
- Grooming, o backlog grooming: Es una reunión que debe realizarse en cada sprint en la que se añaden nuevas user stories y epics, se extraen user stories de las epics existentes y se estima el esfuerzo necesario para las user stories existentes. De esta forma se conoce mejor el estado de lo que aún queda por hacer.
- Retrospectiva, o sprint retrospective: Reunión que se realiza para cerrar un sprint, después incluso de la demo con el cliente. Sirve para ver qué cosas han funcionado, cuáles hay que mejorar, propuestas para el próximo sprint, qué se ha aprendido, y qué problemas bloquean a los desarrolladores. De esta manera, la organización del proyecto. también mejora progresivamente.

# Organización del proyecto
## Interfaces externas
Por un lado, el gestor del proyecto es el encargado de comunicar al grupo de desarrollo con el product owner. Por otro lado, el product owner es el miembro del equipo, perteneciente a la entidad cliente y que se encarga de representar los intereses de su organización respecto al proyecto.

De esta forma, el product owner comunica los requisitos, features y user stories del cliente al lenguaje que maneja el gestor del proyecto, que a su vez traduce toda esta información en tareas que se pueden poner en práctica por parte de los desarrolladores.

## Estructuras internas
Para el desarrollo de esta aplicación, se cuenta con un equipo Scrum de tres miembros dedicado al proyecto con una disponibilidad aproximada de una hora diaria. Este tratará de asemejarse en la mayor medida de lo posible a un equipo real, en cuanto a que pretende ser autoorganizado y multidisciplinar.

### Gestor del proyecto
La gestión del proyecto irá alternandose entre los miembros del equipo de scrum. La figura del gestor de proyecto se le atribuye Scrum master, que en el caso particular de nuestro equipo, cambiará tras la entrega de cada hito.

## Responsabilidades y roles en el proyecto
### Responsabilidades
Las tareas principales que se gestionarán en el proyecto son las siguientes:

1. Trato con usuarios.
2. Reuniones tanto externas como internas.
3. Lectura y revisión de documentación.
4. Elaboración de documentos.
5. Desarrollo y verificación de programas.
6. Formación de usuarios y otras actividades.

Se tratarán de agilizar las responsabilidades 3 y 4 en cuanto a la estructura de la documentación interna a realizar. Se pretende que se realice sobre un soporte digital susceptible de modificarse sobre la marcha del proyecto, que además servirá como herramienta de gestión y consulta de requisitos centralizada. En este mismo sentido, se propone automatizar en la medida de lo posible la generación de documentación de usuario durante el desarrollo de los programas.

### Roles
Debido al reducido tamaño de la organización, los roles dentro del proyecto serán polivalentes e intercambiables entre los miembros del equipo en función de las nencesidades que surjan durante la planificación y desarrollo del proyecto. Los roles básicos identificados como necesarios para nuestro equipo de desarrollo scrum son los siguientes:

- Desarrollador de UX: encargado de diseñar las interfaces de interacción con el usuario y todo aquello relacionado con el frontend de la aplicación.
- Desarrollador de Backend: responsable de crear el modelo y la capa de gestión de datos del proyecto sobre la que desplegar el servicio.
- Responsable de Calidad: se le atribuyen tareas de desarrollo de test plans, creación de documentación de usuario y validación del trabajo de desarrollo respecto a los requisitos identificados.

Además algunas tareas pueden ser atribuídas a cualquiera de los roles anteriores, por ejemplo la generación de los artefactos entregables al cliente, la revisión de merge requests, etc.

# Proceso de gestión
## Inicio
### Estimación
En las metodologías ágiles, como en otras formas de gestión de proyectos, la estimación de la duración de las tareas es un proceso básico e imprescindible. Para ello, usaremos la estimación Fibonacci para valorar la duración de las tareas en forma de storypoints.

Consideraremos que un storypoint es un día de trabajo que, en nuestro caso, equivaldrá aproximadamente a una hora.

### Personal
El personal está compuesto por los tres estudiantes que forman este grupo en la asignatura. Durante el desarrollo no se permitirá la rotación del personal entre proyectos o la inclusión de nuevos desarrolladores al proyecto.

Dada la naturaleza del proyecto y de la organicación, esta restricción no es flexible a cambio y por lo tanto se planificarán las actividades asumiendo que el número de personas asignadas a este proyecto no es variable.

Sin embargo, el personal sí podrá usar horas extra en caso de que haya una desviación respecto a lo planeado y sea necesario para que el proyecto pueda completarse a tiempo. Estas horas extra no supondrán ningún coste a mayores por las características especiales de este proyecto.
=======
<!-- TODO: aportar datos concretos: en cuánto tiempo estimamos el proyecto, nº de sprints -->

### Personal
Se ha seleccionado para este proyecto a tres trabajadores de entre todos los disponibles en la organización. Durante el desarrollo no se permitirá la rotación del personal entre proyectos o la inclusión de nuevos desarrolladores al proyecto.

Dada la naturaleza del proyecto y de la organicación, esta restricción no es flexible a cambio y por lo tanto se planificarán las actividades asumiendo que el número de personas asignadas a este proyecto no es variable.

Sin embargo, el personal sí podrá usar horas extra en caso de que haya una desviación respecto a lo planeado y se necesite para que el proyecto pueda completarse a tiempo. Estas horas extra no supondrán ningún coste a mayores por las características especiales de este proyecto.
>>>>>>> coke

### Adquisición de recursos
Los recursos de personal no solo son gratuitos, sino que incluso deben ser ellos quienes paguen a la Universidad de Valladolid para poder desarrollar este proyecto. Por tanto el coste en recursos humanos del proyecto es cero.

Otros recursos técnicos, organizativos y de despliegue de la aplicación que genere el proyecto tampoco suponen ningún coste para el proyecto puesto que se ha optado por recursos de software libre, software como servicio que ofrece una capa gratuita suficiente, y para la persistencia en el despliegue el cliente pone a nuestra disposición una instancia de MySQL que tampoco supone ningún coste para el proyecto.

En resumen, en este proyecto no es necesario adquirir recursos puesto que todos son gratuitos para el proyecto.

### Entrenamiento del personal
El entrenamiento del personal es el propio de un próximo Graduado en Ingeniería Informática. Los 3 participantes en el proyecto tienen experiencia laboral en metodologías ágiles, con lo que no será necesario un entrenamiento previo en este tipo de metodologías. Además, dada su formación académica y práctica, todos los miembros del equipo también son capaces de desarrollar cualquiera de las tareas técnicas que supone el proyecto. También, en este sentido es relevante destacar que los miembros del equipo tienen experiencia previa con las herramientas que se usarán para la organización y desarrollo del proyecto, o con herramientas similares.

No será necesario ningún entrenamiento a mayores del personal para el desarrollo de este proyecto según lo planeado, ni siquiera sobre el uso de las herramientas.

## Planificación del trabajo
### Actividades del trabajo
Se propone, dentro de la aproximación de Scrum, utilizar para la planificación de las actividades un enfoque basado en User Stories que se definirán internamente sobre nuestra herramienta de gestión en base a las reuniones con el product owner y en ellas figurarán los Acceptance Criteria extraidos del documento de requisitos y serán susceptibles de modificación.

La granularidad de la US pretende ser lo suficientemente amplia como para que sea entendible por el product owner y que recoja cada una de las features por desarrollar desde el punto de vista del usuario final. Para acercar esta visión a la de los desarrolladores, estas user stories se subdividirán en pequeñas subtareas que se identifiquen con labores más técnicas. Los tipos de subtareas que manejaremos serán, principalmente:

- Subtask: Subtarea de diversa naturaleza (control de versiones, gestión de la persistencia y los modelos de datos, codificación, etc.) y la unidad de planifiación más pequeña con la que trabajarán los miembros del equipo.
- Bug: errores detectados sobre alguna de las funcionalidades de la aplicación
- Improvement: labores de mejora del producto (UX, automatización, etc.) de pequeño alcance.

Para organizar el Backlog seguiremos una metodología Kanban, es decir dividiremos el tablero en diferentes columnas, que contendrán las diversas subtareas de cada US. Estas columnas son:

- ToDo: aquellas tareas por ser iniciadas o retomadas por cualquiera de los actores implicados en el proyecto.
- In Progress: toda tarea sobre la que se esté trabajando en un instante
- Resolved: todas las actividades que han sido completadas, pero necesitan de una labor de revisión por algún miembro del equipo de desarrollo
- Done: aquellas tareas que se consideren resueltas, procedentes de cualquiera de las 2 columnas anteriores.

### Asignación de horario (schedule)
Se trabajará normalmente por separado y en remoto, estando coordinados en standups semanales para poner el trabajo en común, pero no se definen unos horarios de trabajo fijo a los que ceñirse por la disponibilidad actual de los miembros del equipo y el alcance del proyecto.

Las tareas se asignarán a un sprint que tendrá fechas de inicio y fin concretas en las que se deben cumplir dichas tareas. También se deberán cumplir tanto los hitos exigidos por el cliente como los hitos definidos por el equipo.

Hemos creado un calendario donde se pueden visualizar todos los eventos relevantes de la programación, y que se puede consultar [aquí](https://calendar.google.com/calendar/embed?src=ul5pnpopqu7r0p0dqje81ep7vc%40group.calendar.google.com&ctz=Europe/Madrid).

### Asignación de recursos
Los recursos pueden ser compartidos sin ningún problema pues los tres desarrolladores tienen la cualificación necesaria para desarrollar cualquier tarea, y los demás recursos son recursos software que permiten el trabajo distribuído en paralelo.

### Asignación de presupuesto
Como hemos descrito, el presupuesto para este proyecto es cero, puesto que no supone ningún coste por estar enmarcado en un entorno educativo y de formación.

## Controles de proyecto
### Control de requisitos
La principal ventaja del enfoque de trabajo ágil elegido es la rápida realimentación de la que se dispone respecto al trabajo realizado, de modo que el producto va ganando funcionalidad que es validada al mismo ritmo, pues al final de cada sprint, se comprueba que el trabajo realizado (que será una pequeña fracción del total) se corresponde con las necesidades del cliente, y en los casos de desviaciones y aparición de nuevos requisitos, en la sesión posterior a esta reunión de demostración se realiza una sesión de backlog grooming, en la que se modifica el contenido del backlog para actualizar o añadir nuevas user stories respectivamente.

### Control de planificación
Al igual que para el control de los requisitos, y de cara a la planificación interna del proyecto, así como la que se coordine con el cliente, se realizará una sesión de retrospectiva, aproximadamente a la mitad del proyecto para comprobar la salud general del proyecto, poner en común las opiniones del equipo de desarrollo y tratar de corregir los errores de pasados sprint, imponiendo medidas para evitar estas situaciones en el ultimo tramo del proyecto.

### Control de presupuesto
Dado que no se maneja un presupuesto en este proyecto por su carácter académico, como se ha explicado ya, no se consideran medidas de control del presupuesto, aunque en un proyecto de mayor alcance, se podría plantear la simulación de costes de desarrollo y aprovisionamiento.

### Control de calidad
No aplica.

### Informes del proyecto y comunicación
Mediante la estructuración en sprints del proyecto, cada uno de ellos culminando en una demostración del valor aportado durante el sprint al producto, se establece el principal canal de comunicación con el product owner, pues se pretende que estas demos sean sesiones abiertas en la que cabe la opinión de cualquier miembro del equipo y en la que el equipo de desarrollo obtiene la realimentación de su trabajo, de forma que tras la demo disponga de la información necesaria como para rellenar el Backlog con nuevas cuestiones, cerrar el sprint terminado y configurar el nuevo que ha de iniciarse.

### Recolección de métricas
Gracias a la elección de la herramienta de planificación y seguimiento del proyecto que ya hemos mencionado, disponemos de resúmenes del progreso del proyecto, que nos servirán para determinar las desviaciones sobre la planificación inicial.

Durante la sessión de spring planning, posterior a cada una de las demos con el cliente, los miembros del equipo votarán y decidirán, por acuerdo, los story points en base a la estimación del trabajo que les suponga cada una de las user stories incluidas en el sprint. Esta estimación servirá, según las subtareas se vayan completando, para determinar la velocity de cada uno de los sprints y de los actores implicados en el desarrollo.

Con estos datos, generaremos el burndown chart que nos permitirá controlar el progreso del proyecto viendo el ritmo al que se completan tareas valorando el conjunto de la duración restante, y si se corresponde con la predicción y el progreso necesarios para alcanzar los distintos hitos a tiempo.

## Gestión de riesgos
Pese a que la planificación se ha hecho pensando en un caso ideal, también se trata la posibilidad de que puedan ocurrir situaciones imprevistas que potencialmente puedan causar un incumplimiento de los objetivos ya sea de grano más fino o incluso de grano más grueso. La gravedad de estos riesgos varía en función del tipo de riesgo y del estado del proyecto. El tratamiento dado a los riesgos se especificará en la evolución del proyecto a medida que surjan.

Algunos ejemplos de riesgos posibles en nuestro proyecto son los siguientes:

- Fallo de la infrastructura de aulas virtuales de la ESII, suceso que imposibilitaría la entrega de los hitos.
- Enfermedad de alguno de los miembros, lo cual causaría modificaciones en la planificación del proyecto.
- Fallo de la infrastructura propia del proyecto como la base de datos MySQL, GitLab u otros servicios empleados.
- Cambio en los requisitos planteados inicialmente por el cliente.
- Pérdida de datos debida a fallos de hardware.
- Imposibilidad de realizar las tareas planeadas en remoto debido a problemas con la conexión a Internet doméstica de alguno de los miembros del equipo.

En aquellos casos en los que el compromiso interno del equipo no se cumpliese por algún motivo, se le comunicará esta situación al product owner durante la demo con la que se concluye el sprint, siendo específicos con los objetivos no alcanzados y las posibles razones.

## Cierre del proyecto
El cierre del proyecto se alcanzará al finalizar el ultimo sprint, que coincide temporalmente con el tercer hito del proyecto, sobre el que se programa una sesión de Demo del producto final con todas las partes implicadas en el desarrollo y se realizará el despliegue de la rama master del proyecto sobre un servidor de producción. Simulando el momento a partir del cuál la aplicación podría entrar en explotación.

# Proceso técnico
## Modelo de Proceso
En un desarrollo iterativo e incremental el proyecto se planifica en diversas iteraciones o bloques temporales en nuestro caso, sprints quincenales.

Las iteraciones se pueden entender como miniproyectos: en todas las iteraciones se repite un proceso de trabajo similar para proporcionar un resultado completo sobre producto final, de manera que el cliente pueda obtener los beneficios del proyecto de forma incremental. Para ello, cada requisito se debe completar en una única iteración y además, de esta manera no se deja para el final del proyecto ninguna actividad relacionada con la entrega de requisitos vitales del proyecto.

En cada iteración el equipo evolucionará el producto a partir de los resultados completados en las iteraciones anteriores, añadiendo nuevos objetivos o mejorando los que ya fueron completados. Un aspecto fundamental para guiar el desarrollo iterativo e incremental es el de priorizar algunos objetivos en función del valor aportado al cliente.

## Métodos, Herramientas y Técnicas
### Métodos
- Scrum: Es una metodología ágil basado en el desarrollo incremental e iterativo. El scrum es una metodología flexible que permite la introducción de cambios de requisitos durante el desarrollo.
- Kanban: Es un sistema de información en el que mediante tarjetas se controla el tiempo de desarrollo, el estado y las tareas del proyecto.

### Herramientas
Para desarrollar la metodología que estamos describiendo, necesitaremos un sistema de control de versiones, un repositorio de código canónico y un software de gestión de proyectos adaptado a metodologías de desarrollo ágil. Hemos elegido las siguientes herramientas:

- Git: El control de versiones es una herramienta básica en cualquier proyecto en el que este implicado el software. No sólo por la facilidad que da a la hora de sincronizar equipos de trabajo y programación en paralelo sino también por la posibilidad de volver a versiones anteriores o buscar el cambio en el que se introdujo un bug. Entre las tecnologías que nos ofrece el mercado hemos decidido utilizar git por su facilidad de uso, la experiencia del equipo con esta tecnología y su extensión entre la comunidad de programadores.
- GitLab: Para poder compartir el código entre nosotros y para evitar las pérdidas de código debido a problemas imprevistos en el proyecto se hará uso de un servidor de control de versiones. Se ha optado por Gitlab debido a que es software libre que permite crear repositorios privados en sus servidores de forma gratuita y sin restricciones a diferencia de otras alternativas freemium como Github o Bitbucket.
- Taiga.io: Es un gestor de proyectos en la nube que nos permite aplicar las metodologías scrum elegidas para el desarrollo de nuestro poryecto. Su elección se debe al resultado de la búsqueda de alternativas de software libre a gestores de proyectos como Jira.
- Google Calendar: Sistema visual de calendarización proporcionado por Google.

### Técnicas
Se propone utilizar un proceso de desarrollo basado en la definición de subtareas, por parte de los diferentes miembros del proyecto actuando como reporters, sobre las user stories del backlog.

Cada miembro del equipo de desarrollo seguirá el workflow de asignarse las subtareas sobre las que trabajará y, en consecuencia, mover la tarea de la columna ToDo a la columna In Progress del Backlog. Una vez finalizada una subtarea, se situará en la columna Done, excepto aquellas que se refieran a tareas susceptibles de revisión (merge request, Bugs, etc.) que quedarán a la espera de este proceso en la columna Resolved.

Para el desarrollo en el repositorio usaremos la estrategia de branching para git descrita en [A successful Git Branching Model](http://nvie.com/posts/a-successful-git-branching-model/). Contaremos, por lo tanto, con las siguientes ramas en el repositorio:

- `master`: la rama principal que almacena el código funcional.
- `develop`: la rama sobre la que se trabajará. Puede que sea inestable en algunos momentos. Se irá mezclando con `master` progresivamente.
- Una rama por cada user story o feature sobre la que se desarrolle, que después se mezclará con `develop`.
- Una rama por cada bug detectado sobre la rama `develop`.

El objetivo de esta estrategia es que todo el código que haya en `master` sea siempre funcional, para que pueda probarse o mostrarse a cualquiera de los stakeholders en todo momento.

## Infraestructura del Proyecto
La infrastructura del proyecto consta de una parte proporcionada por el cliente, consistente en un servidor MySQL necesario para suplir la necesidad de persistencia de datos dentro del proyecto; y por otro lado, de una infrastructura externa consistente en servicios online proporcionados por terceros, tales como los mencionados anteriormente: GitLab, Taiga, etc.

## Aceptación de Producto
Como se comentaba en la sección de controles del proyecto, existen sesiones de demostración en las que las diferentes iteraciones que añadan valor a la aplicación serán mostradas y validadas con los stakeholders, de forma que al llegar al final del proceso de desarrollo, siempre y cuando no se hayan producido desviaciones de la planificación, la aceptación del producto esté condicionada a una última sesión de demostración, pues el grueso de las funcionalidades ya habrán sido validadas en sesiones anteriores.

# Procesos de apoyo
## Gestión de configuraciones
Utilizaremos git, un sistema de control de versiones; con Gitlab, un servidor de git con licencia de software libre que nos servirá como repositorio centralizado, de modo que, como se comentaba en la sección anterior, podamos llevar el control sobre los cambios realizados en el proyecto y gestionar las diferentes versiones del producto.

## Verificación y validación independiente
No aplica.

## Documentación
Durante el desarrollo del proyecto se generan diversos documentos mediante la herramienta de gestión elegida que nos permiten visualizar internamente el estado y el progreso del mismo. Algunos de esos documentos internos son:

- Informes de sprint: Es el documento que se genera en las retrospectivas y donde se refleja todo aquello que salió bien, salió mal, se aprendió, se puede mejorar, etc.
- Burndown report: Es un documento en el que se puede visualizar el ritmo de trabajo durante el sprint con herramientas como el burndown chart, tanto de las tareas previstas para el sprint como del proyecto en su conjunto.
- Velocity chart: Es una herramienta que permite ver la cantidad de trabajo que se ha podido llevar a cabo en el sprint, de forma que se pueda estimar cada vez de forma más realista cuánto trabajo puede llevar a cabo el equipo.

Otro tipo de documentos son aquellos destinados a ser entregados al cliente de forma que éste pueda conocer el estado del proyecto. Dichos documentos han sido definidos y exigidos por el propio cliente y son:

- El presente plan de proyecto.
- El documento de análisis y diseño de la aplicación.
- El documento de implementación.
- Un documento de seguimiento del proyecto, incluyendo las medidas tomadas durante el desarrollo del mismo.
- El manual de usuario y manual de despliegue para la aplicación generada.

## Garantía de la calidad (Quality Assurance)
Dentro de las acciones de garantía de la calidad, realizaremos pruebas manuales periódicas en función de los test plans definidos por los responsables de calidad sobre las user stories para comprobar que la funcionalidad ha sido implementada correctamente y se ajusta a los requisitos, pero también para comprobar que no se ha introducido ninguna regresión en el código. En los casos de detectar defectos de funcionalidad, estos se registrarán como bugs en la plataforma de seguimiento, aportando toda la información necesaria para su identificación:

- Pasos para su reproducción
- Entorno sobre el que se ha detectado
- Resultado esperado
- Resultado actual defectuoso

Además, se proyecta la realización algunos tests unitarios y funcionales automáticos si se detectan funcionalidades críticas en la aplicación.

Como una técnica más para garantizar la calidad de los cambios introducidos en el repositorio, emplearemos una estrategia de code review, mediante la cual, cada merge request realizado sobre la rama develop, será revisado por un desarrollador diferente al que lo creó, a fin de mejorar la calidad del producto intermedio. Y por último, aprovechando la funcionalidad del servidor de git elegido, algunos errores de codificación identificados se reportarán mediante issues en el mismo repositorio central. Estas sólo se diferencian de los bugs que se registren en taiga por referirse específicamente al código.

## Revisiones y Auditorías
No aplica.

## Resolución de problemas
No aplica.

## Gestión de subcontratas
No se realizarán subcontratas.

### Proceso de subcontrata
No es necesario definirlo.

### Monitorización del rendimiento de las subcontratas
No es necesario definirlo.

## Mejora de procesos
No aplica.

### Iniciativa de mejora de procesos de software o sistemas
No aplica.

### Grupo de procesos de ingeniería de sistemas
No aplica.

#ANEXO I: Referencias bibliográficas:

1. [Scrum guide](http://www.scrumguides.org/docs/scrumguide/v1/scrum-guide-us.pdf)
2. [Documentación de taiga](https://github.com/taigaio/taiga-doc)
3. [A successful Git Branching Model](http://nvie.com/posts/a-successful-git-branching-model/)
4. [Especificación de la práctica](https://aulas.inf.uva.es/pluginfile.php/26116/mod_resource/content/6/materiales_planif/PGP_survey3.pdf)
5. [A successful Git Branching Model](http://nvie.com/posts/a-successful-git-branching-model/)
