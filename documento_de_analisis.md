% Proyecto GTPROS: Documento de análisis y diseño
% Adrián de la Rosa Martín; Samuel Alfageme Sanz; Jorge Cuadrado Saez
% 8 de diciembre de 2015

# Introducción
En este documento, requerido por el cliente, especificamos el modelo de dominio, los casos de uso, el diagrama de casos de uso, algún diagrama de secuencia de algún caso de uso destacado y otros modelos complementarios.

Toda la información ha sido extraida del documento de requisitos recibido al inicio del proyecto.

# Modelo de dominio
Adjuntamos el modelo de dominio en este mismo documento.

![Modelo de dominio](Modelos/MD.png)

# Diagrama entidad-relación
En base al modelo de dominio, hemos creado el siguiente diagrama entidad-relación que servirá de base para crear la base de datos.

![Diagrama entidad-relación](Modelos/DER.png)

# Requisitos
## Requisitos funcionales
1. El sistema deberá permitir trabajar con proyectos a partir de la planificación de actividades implicadas en el proyecto.
2. El sistema deberá permitir la creación de hitos con una duración de 0.
3. El sistema deberá permitir a los gestores de proyecto el seguimiento temporal de los proyectos.
4. El sistema deberá permitir obtener resumenes de los proyectos en curso o ya finalizados y cerrados.
5. El sistema deberá permitir a los gestores del proyecto consultar las actividades en curso y el esfuerzo dedicado a ellas hasta el momento por cada recurso humano asignado.
6. El sistema deberá permitir el uso de datos temporales de proyectos ya finalizados para planificar y presupuestar nuevos proyectos.
7. El sistema deberá permitir dar de alta proyectos.
8. El sistema deberá permitir dar de alta trabajadores.
9. El sistema deberá permitir asignar trabajadores a proyectos.
10. El sistema deberá permitir al jefe de proyecto finalizar una actividad, etapa o un proyecto.
11. El sistema deberá proporcionar al jefe de proyecto los informes oportunos mientras el proyecto este abierto.
12. El sistema deberá permitir a los desarrolladores la introducción de los datos de sus tareas personales.
13. El sistema deberá marcar el tiempo por encima del estipulado en el requisito no funcional 1 como tiempo sobreasignado en la tarea correspondiente.
14. El sistema deberá generar resumenes globales de proyectos ya cerrados.
15. El sistema deberá permitir configurar el número de proyectos máximo en el que un trabajador puede estar trabajando simultáneamente.
16. El usuario deberá poder modificar los datos de actividades abiertas en un proyecto en curso.
17. El sistema deberá permitir a los empleados crear su propio calendario.
18. El sistema deberá tener en cuenta los calendarios personales de los empleados cuando el jefe de proyecto cree una planificación.
19. El sistema permitirá al empleado elegir sus periodos de vacaciones en cualquier momento.
20. El sistema no permitirá la elección de días de vacaciones en periodos en los que el trabajador tenga actividades asignadas.
21. El sistema no permitirá la modificación de periodos de vacaciones ya asignados.
22. El sistema permitirá al jefe de proyecto aceptar o rechazar informes de actividades.

## Requisitos no funcionales
1. La suma de las estimaciones de las tareas de un trabajador no podrá superar las 40 horas semanales.
2. El responsable de proyecto sólo podrá serlo de un proyecto a la vez.
3. El porcentaje de tiempo invertido en un proyecto por parte de un desarrollador no podra ser superior al 40% del tiempo total invertido.
4. El desarrollador sólo podrá consultar los datos correspondientes a actividades activas o cerradas que le han sido asignadas en los proyectos abiertos en ese momento.
5. Un desarrollador debe realizar 40 horas a la semana.
6. El máximo por defecto de proyectos simultáneos en los que puede estar un trabajador será dos.
7. La interfaz del sistema será un navegador web.
8. El sistemá podrá usarse desde cualquier navegador.
9. Un empleado tiene derecho a 4 semanas de vacaciones que podrá disfrutar en 2 periodos de semanas completas.
10. Un empleado no podrá tener más de 48 tareas personales entre todos los proyectos en los que este trabajando. El trabajador no podrá estar implicado en más de 4 actividades durante una semana.
11. El porcentaje de Actividades asignadas a un empleado en un proyecto será por defecto del 100%.

## Requisitos de información
1. El sistema almacenará por cada actividad un identificador, una descripción, un rol asignado, actividades predecesoras, duración temporal estimada en horas hombre y fecha de inicio y de fin estimadas.
2. En una tarea terminada adicionalmente se almacenará el esfuerzo estimado y el esfuerzo real.
3. El sistema almacenará por cada proyecto una persona responsable asignada, una tabla de roles, una tabla de categorías, calendarización, actividades, las personas asignadas al proyecto y su porcentaje de participación asignado.
4. El sistema almacenará un calendario de tareas personales por cada empleado.
5. El sistema almacenara informes de las actividades por semanas y su estado (aceptado, rechazado o pendiente).

# Casos de uso
## UC01 - Acceder a la aplicación
### Usuarios objetivo
Usuario del Sistema.

### Precondición
- El usuario no está logeado en el sistema.
- El usuario está registrado en el sistema.

### Postcondición
- El usuario tiene acceso al sistema.

### Descripción
1. El usuario accede a la vista de login.
2. El sistema muestra el formulario.
3. El usuario introduce los datos.
4. El sistema comprueba su validez y muestra la vista correspondiente al rol de ese usuario.

### Flujo alternativo
- 3.1. El usuario cancela el login.
- 4.1. El sistema comprueba que los datos no son válidos, muestra un error y vuelve al paso 2.

## UC02 - Dar de alta trabajadores
### Usuarios objetivo
Administrador del sistema.

### Precondición
- El usuario está logeado en el sistema.
- El usuario es un administrador del sistema.

### Postcondición
- Hay un nuevo usuario en el sistema.

### Descripción
1. El usuario administrado inicia la creación de un nuevo usuario.
2. El sistema muestra un formulario con los datos requeridos.
3. El usuario administrador completa el formulario y lo envía.
4. El sistema valida los datos y pide confirmación.
5. El usuario administrador confirma la creación.
6. El sistema crea el nuevo usuario.

### Flujo alternativo
- 3.1. El usuario cancela la creación. El caso de uso queda sin efecto.
- 4.1. El Sistema detecta que los datos no son válidos. El caso de uso continúa en el paso 3.
- 5.1. El usuario no valida los datos. El caso de uso continúa en el paso 3.

## UC03 - Dar de alta proyectos
### Usuarios objetivo
Administrador del sistema.

### Precondición
- El usuario está logeado en el sistema.
- El usuario es un administrador del sistema.

### Postcondición
- Hay un nuevo proyecto dado de alta en el sistema.

### Descripción
1. El usuario administrador inicia la creación del proyecto.
2. El sistema pide los datos necesarios para dar de alta el proyecto.
3. El usuario administrador introduce el responsable del proyecto y carga la tabla de roles y la tabla de categorías.
4. El sistema valida los datos introducidos y pide la confirmación al usuario.
5. El usuario administrador confirma el alta del proyecto.
6. El sistema da de alta el proyecto.

### Flujo alternativo
- 3.1. El usuario cancela el alta del proyecto. El caso de uso queda sin efecto.
- 4.1.1 El Sistema detecta que los datos no son válidos. El caso de uso continúa en el paso 3.
- 4.2.1 El Sistema detecta que el responsable del proyecto excede el número de proyectos de los que puede ser responsable. El caso de uso continúa en el paso 3.
- 5.1. El usuario no confirma el alta. El caso de uso continúa en el paso 3.

## UC04 - Configurar la aplicación
### Usuarios objetivo
Administrador del sistema.

### Precondición
- El usuario está logeado en el sistema.
- El usuario es un administrador del sistema.

### Postcondición
- La configuración de la aplicación ha cambiado.

### Descripción
1. El usuario administrador inicia el cambio de la configuración.
2. El sistema muestra todas las opciones de configuración.
3. El usuario administrador modifica las opciones de configuración.
4. El sistema valida que las opciones de configuración estén en un estado correcto y cambia la configuración.

### Flujo alternativo
- 3.1. El usuario cancela el alta del proyecto. El caso de uso queda sin efecto.
- 4.1. El Sistema detecta que la configuración no es válida. El caso de uso continúa en el paso 3.

## UC05 - Inicializar un proyecto asignado
### Usuarios objetivo
Jefe de Proyecto.

### Precondición
- El usuario está logeado en el sistema.
- El usuario es un Jefe de Proyecto.

### Postcondición
- Se ha editado la información del proyecto.

### Descripción
1. El usuario jefe de proyecto inicia la edición del proyecto.
2. El sistema muestra los proyectos dados de alta a los que ha sido asignado.
3. El usuario jefe de proyecto selecciona el proyecto.
4. El sistema solicita las personas implicadas y su porcentaje de participación.
5. El usuario jefe de proyecto introduce los usuarios y sus porcentajes.
6. El sistema valida los datos y pide que se cargue un plan de proyecto.
7. El usuario jefe de proyecto introduce el plan de proyecto.
8. El sistema valida los datos y pide la relación de actividades con sus datos.
9. El usuario jefe de proyecto introduce las actividades con su duración, precedencias y rol asociado.
10. El sistema valida los datos y pide la calendarización de las actividades.
11. El usuario jefe de proyecto introduce la calendarización.
12. El sistema valida los datos y pide la asignación de personas a cada actividad.
13. El usuario jefe de proyecto asigna personas a cada actividad.
14. El sistema valida los datos y pide confirmación.
15. El usuario confirma la creación del proyecto.
16. El sistema introduce los datos en el proyecto.

### Flujo alternativo
- 3.1., 5.1.1, 7.1.1, 9.1.1, 11.1.1, 13.1.1, 15.1.1 El usuario cancela el alta del proyecto. El caso de uso queda sin efecto.
- 5.2.1, 7.2.1, 9.2.1, 11.2.1, 13.2.1, 15.2.1 El usuario vuelve al paso anterior. El caso de uso continúa en el paso anterior.
- 6.1, 8.1, 10.1, 12.1, 14.1 El Sistema detecta que los datos no son válidos. El caso de uso continúa en el paso anterior.

## UC06 - Finalizar actividades o proyectos liderados
### Usuarios objetivo
Jefe de Proyecto.

### Precondición
- Existe al menos un proyecto iniciado.
- El usuario está logeado en el sistema.
- El usuario es jefe de proyecto del proyecto que se desea finalizar.

### Postcondición
- El proyecto elegido queda marcado como finalizado.

### Descripción
1. El usuario jefe de proyecto selecciona el proyecto o actividad que desea finalizar.
2. El sistema muestra la información del proyecto o actividad y sus opciones.
3. El usuario jefe de proyecto selecciona la opción de finalizar.
4. El sistema valida que el proyecto o actividad puede finalizarse y pide confirmación.
5. El usuario jefe de proyecto confirma el cierre del proyecto o actividad.

### Flujo alternativo
- 3.1, 5.1 El usuario cancela el caso de uso. El caso de uso queda sin efecto.
- 4.1 El sistema detecta que no puede finalizarse el proyecto o actividad y muestra la información al respecto. El caso de uso queda sin efecto.

## UC07 - Crear Actividades en un proyecto asignado
### Usuarios objetivo
Jefe de Proyecto.

### Precondición
- El usuario debe estar logeado.
- El usuario debe ser jefe de proyecto del proyecto seleccionado.
- Debe existir al menos un proyecto asignado al usuario jefe de proyecto.

### Postcondición
- El proyecto tiene una nueva actividad.

### Descripción
1. El usuario jefe de proyecto selecciona el proyecto.
2. El sistema muestra la información del proyecto y sus opciones.
3. El usuario jefe de proyecto selecciona la opción añadir actividad.
4. El sistema pide los datos encesarios para crear la actividad.
5. El usuario proporciona los datos requeridos.
6. El sistema valida los datos y pide confirmación.
7. El usuario confirma la creación de la actividad.

### Flujo alternativo
- 3.1, 5.1, 7.1 El usuario cancela el caso de uso. El caso de uso queda sin efecto.
- 6.1 El sistema detecta que los datos no son validos. El caso de uso continua en el paso 5.

## UC08 - Consultar datos de proyectos liderados
### Usuarios objetivo
Jefe de Proyecto.

### Precondición
- El usuario está logeado en el sistema.
- El usuario es jefe de proyecto del proyecto seleccionado.
- El proyecto seleccionado está abierto.
- El usuario tiene al menos un proyecto asignado.

### Postcondición
- El usuario recibe un informe del proyecto.

### Descripción
1. El usuario jefe de proyecto selecciona el proyecto.
2. El sistema muestra la información del proyecto y sus opciones.
3. El usuario jefe de proyecto selecciona la opción de consultar datos.
4. El sistema muestra los datos del proyecto.
5. El usuario finaliza la visualización de los datos.

### Flujo alternativo
- 3.1 El usuario cancela el caso de uso. El caso de uso queda sin efecto.

## UC09 - Reutilizar datos de proyectos previos
### Usuarios objetivo
Jefe de Proyecto.

### Precondición
- El usuario está logeado en el sistema.
- El usuario es jefe de proyecto del proyecto seleccionado.
- El usuario tiene al menos un proyecto asignado.
- Los proyectos reutilizados están cerrados.

### Postcondición
- Datos de un proyecto cerrado han sido reutilizados.

### Descripción
1. El usuario jefe de proyecto selecciona reutilizar información de proyectos previos.
2. El sistema muestra los proyectos previos con su información.
3. El usuario jefe de proyecto selecciona los datos que desee de cada proyecto.
4. El sistema carga las datos al proyecto actual y muestra los lugares donde es necesario introducir datos concretos de este proyecto.
5. El usuario introduce esos datos concretos del proyecto.
6. El sistema valida los datos y pide confirmación.
7. El usuario confirma la reutilización de datos.

### Flujo alternativo
- 2.1 El sistema no encuentra proyectos previos finalizados y muestra una lista vacía con un mensaje explicativo. El caso de uso queda sin efecto.
- 3.1, 5.1, 7.1 El usuario cancela la acción. El caso de uso queda sin efecto.
- 4.1 El sistema detecta que no es necesario introducir información concreta para el proyecto actual. El caso de uso continúa en el paso 6.
- 6.1 El sistema detecta que los datos introducidos no son validos, muestra un error. El caso de uso continúa en el paso 5.

## UC10 - Crear tareas personales
### Usuarios objetivo
Desarrollador.

### Precondición
- El desarrollador está autenticado en el sistema.
- El desarrollador está gestionando uno de sus proyectos asignados.

### Postcondición
### Descripción
1. El usuario informa al sistema de que desea una tarea personal en uno de sus proyectos.
2. El sistema pide al usuario la información necesaria para crearla.
3. El usuario introduce los datos de la nueva tarea personal.
4. El sistema crea la tarea e informa al usuario de que ha sido creada satisfactoriamente.

### Flujo alternativo
- 4.1 Si los datos son erróneos, el sistema informa al usuario del error y el caso de uso finaliza sin efecto.

## UC11 - Introducir tiempo de trabajo en tareas personales
### Usuarios objetivo
Desarrollador.

### Precondición
- El desarrollador está autenticado en el sistema.
- El desarrollador ha finalizado una tarea personal.
- El desarrollador está gestionando uno de sus proyectos asignados.

### Postcondición
- Se ha introducido tiempo en una tarea personal.

### Descripción
1. El usuario desarrollador selecciona la tarea que ha finalizado.
2. El sistema muestra la información y opciones disponibles para la tarea.
3. El usuario desarrollador selecciona introducir tiempo de trabajo.
4. El sistema pide que se introduzca el tiempo de trabajo.
5. El usuario desarrollador introduce el tiempo.
6. El sistema valida los datos e introduce el tiempo.

### Flujo alternativo
- 3.1, 5.1 El usuario cancela el caso de uso. El caso de uso queda sin efecto.
- 6.1 El sistema detecta que el usuario desarrollador ha realizado más horas del máximo semanal y marca el tiempo de más como sobreasignado.

## UC12 - Consultar datos de actividades
### Usuarios objetivo
Desarrollador.

### Precondición
- El desarrollador está autenticado en el sistema.
- El desarrollador tiene proyectos asignados.

### Postcondición
### Descripción
1. El usuario indica al sistema el proyecto cuyas actividades desea consultar.
2. El sistema muestra la información disponible sobre las actividades del proyecto seleccionado.

### Flujo alternativo

## UC13 - Acceder a resúmenes globales de proyectos finalizados
### Usuarios objetivo
Desarrollador.

### Precondición
- El desarrollador está autenticado en el sistema.

### Postcondición
### Descripción
1. El usuario indica que desea ver un resumen global de un proyecto finalizado de la empresa.
2. El sistema muestra los proyectos finalizados disponibles.
3. El usuario selecciona el resumen que desea ver.
4. El sistema muestra el resumen seleccionado.

### Flujo alternativo

## UC14 - Obtener informes de Actividad
### Usuarios objetivo
Trabajador.

### Precondición
- El proyecto debe estar abierto.

### Postcondición
### Descripción
1. El usuario selecciona el proyecto abierto cuyos informes desea consultar.
2. El sistema muestra los informes disponibles del proyecto.
3. El usuario selecciona el informe que desea visualizar.
4. El sistema muestra el informe al usuario.

### Flujo alternativo

## UC15 - Rellenar calendario personal
### Usuarios objetivo
Trabajador.

### Precondición
- El trabajador está autentificado en el sistema.

### Postcondición
- Los datos sobre las vacaciones asociados al trabajador han sido actualizados.

### Descripción
1. El usuario indica al sistema que desea modificar su calendario personal.
2. El sistema muestra el calendario.
3. El usuario selecciona en el calendario los días que quiere elegir como vacaciones.
4. El sistema comprueba que los días seleccionados son compatibles con la política de la empresa y informa al usuario de que la información se ha guardado correctamente.

### Flujo alternativo
- 4.1 Si los días seleccionados no son compatibles con la política de la empresa, se informará al usuario del error y el caso de uso terminará sin efecto.

## UC16 - Acceder y aprobar informes de Actividad
### Usuarios objetivo
Jefe de proyecto.

### Precondición
- El usuario está logeado.
- El usuario es jefe de proyecto del proyecto seleccionado.
- El usuario tiene al menos un proyecto asignado.

### Postcondición
- El informe seleccionado queda marcado como validado.

### Descripción
1. El usuario jefe de proyectos selecciona la opción de ver informes.
2. El sistema muestra los informes del proyecto.
3. El usuario selecciona un informe.
4. El sistema muestra el informe y sus opciones.
5. El usuario selecciona la opción de validar.
6. El sistema marca el informe como validado.

### Flujo alternativo
- 2.1 El sistema no encuentra informes sin validar y muestra un texto explicativo. El caso de uso finaliza.
- 3.1, 5.1 El usuario cancela el caso de uso. El caso de uso queda sin efecto.

## UC17 - Obtener informes de Proyecto
### Usuarios objetivo
Jefe de proyecto.

### Precondición
- El usuario está autentificado en el sistema.

### Postcondición
### Descripción
1. El usuario indica el proyecto sobre el cual desea obtener informes.
2. El sistema muestra la información y tipos de informes disponibles para ese proyecto.
3. El usuario indica el tipo de informe que desea obtener.
4. El sistema muestra el informe elegido. Puntos de extensión UC18, UC19 y UC20.

### Flujo alternativo

## UC18 - Relación temporal de Trabajadores y sus Actividades asignadas
### Usuarios objetivo
Jefe de proyecto.

### Precondición
- Las actividades deben estar activas.
- El usuario está autentificado en el sistema.

### Postcondición
### Descripción
1. El sistema recoge todas las Actividades abiertas en el Proyecto de los Trabajadores del mismo.
2. El sistema genera el informe y lo muestra.

### Flujo alternativo

## UC19 - Relación temporal de Trabajadores y sus recursos
### Usuarios objetivo
Jefe de proyecto.

### Precondición
### Postcondición
### Descripción
1. El sistema recoge toda la información sobre los recursos de los Trabajadores del Proyecto.
2. El sistema genera el informe y lo muestra.

### Flujo alternativo

## UC20 - Informes de Proyectos finalizados
### Usuarios objetivo
Jefe de proyecto.

### Precondición
### Postcondición
### Descripción
1. El sistema recoge la información sobre el Proyecto finalizado requerido.
2. El sistema genera el informe y lo muestra.

### Flujo alternativo

# Roles
Definimos los siguientes cuatro roles:

- Administrador
- Desarrollador
- Jefe de Proyecto
- Usuario.

En el diagrama de casos de uso se puede observar la herencia entre estos roles y qué casos de uso puede desarrollar cada uno de ellos.

# Diagrama de casos de uso
Adjuntamos el diagrama de casos de uso en este mismo documento.

![Diagrama de casos de uso](Modelos/DCU.png)

# Historias de usuario
Puesto que estamos desarrollando el proyecto con una metodología ágil como es Scrum, hemos tenido que poner los casos de uso y los requisitos en forma de historias de usuario de alto nivel que faciliten la comunicación entre los desarrolladores y el product owner.

## Como administrador del sistema quiero dar de alta proyectos
AC:

* Asignar Responsable del Proyecto
* Cargar datos iniciales:
    * Tablas de roles
    * Tablas de categorías

## Como administrador del sistema quiero dar de alta trabajadores
AC:

* Cuenta del trabajador
* Categoría

### Relacion de Roles:
- Jefe de proyecto (1)
- Analista (2)
- Diseñador (3)
- Analista-Programador (3)
- Responsable equipo de pruebas (3)
- Programador (4)
- Probador (4)

## Como jefe de proyecto quiero obtener informes mientras mi proyecto esté abierto
La herramienta permitirá a los gestores de un proyecto en curso consultar las actividades que se están realizando y el esfuerzo (en horas/hombre) dedicado a cada una de ellas por los diferentes recursos humanos asignados a dichas actividades.

La herramienta permitirá a cada gestor de proyecto obtener informes de seguimiento temporal, a partir de los datos introducidos por los trabajadores adscritos del proyecto que lidera mientras esté en alguna fase de su desarrollo.

- Relación de trabajadores y sus actividades asignadas durante un periodo (semanal) determinado.
- Relación de trabajadores con **informes de actividad** pendientes de envío y fechas de los mismos.
- Relación de trabajadores con **informes de actividad** pendientes de aprobación y fechas de los mismos.
- En estos dos últimos se podrá acceder a los informes concretos a partir de la relación anterior.
- Relación de actividades activas o finalizadas en una fecha concreta o en un periodo de tiempo, viendo el tiempo planificado y el tiempo realizado de cada una de ellas. Conviene recordar que es el jefe de proyecto el que determina la finalización de una actividad.
- Relación de actividades que han consumido o están consumiendo más tiempo del planificado.
- Actividades a realizar, así como los recursos asignados, durante un periodo de tiempo determinado posterior a la fecha actual.
- Relación de personas, con las actividades asignadas y los tiempos de trabajo para cada una de ellas para un periodo de tiempo posterior a la fecha actual (la semana siguiente, la quincena siguiente, etc).
- Al cerrar el proyecto se podrán obtener informes sobre cómo ha ido la realización del mismo.

## Como trabajador quiero tomar datos de mi trabajo en las actividades activas
La herramienta permitirá tomar datos del trabajo realizado (solo se trabaja de lunes a viernes de cada semana), únicamente de actividades activas en ese periodo de tiempo considerado en el que se encuentre el proyecto y el rol de la persona que se haya identificado.

El usuario solo podrá modificar los datos que haya introducido de actividades activas dentro de un proyecto en curso, mientras dichas actividades no estén cerradas.

El número de “tareas personales” que una persona puede realizar por semana no debe superar las 24 entre todos los proyectos en los que esté trabajando durante la misma. Esto supone que no puede estar implicada en más de 4 actividades durante una semana.

### Relación de tareas personales:

1. Trato con usuarios (llamadas, citas individuales, etc.)
2. Reuniones tanto externas como internas.
3. Lectura y revisión todo tipo de documentaciónA (externa o generada por el proyecto).
4. Elaboración de documentación (informes, documentos, documentación de programas).
5. Desarrollo y verificación de programas
6. Formación de usuarios y otras actividades (sin clasificar).

## Como jefe de proyecto quiero utilizar datos de proyectos previos para planificar y presupuestar proyectos
Permitir que los gestores de proyectos utilicen los datos temporales de proyectos finalizados (cerrados) para ayudar a planificar y presupuestar los nuevos y para analizar las prácticas de trabajo seguidas con el fin de servir de ayuda en cualquier proceso de mejora del proceso de desarrollo de software. Una vez finalizada una tarea existiran dos datos sobre el esfuerzo (el estimado y el real obtenido).

## Como jefe de proyecto quiero gestionar el inicio de mis proyectos
AC:

- Asignar personas al proyecto y su porcentaje de participación (Default: 100% ; nunca mayor que este valor)
- Cargar un plan de proyecto (upload)
- Relación de actividades
    - Duración estimada
    - Actividades precedentes
    - Rol asociado
- Calendarización
- Asignación de personas (plural) a actividad  (en función del rol de la persona y de la actividad)

## Como jefe de proyecto quiero poder consultar los datos de cualquiera de mis proyectos en curso y pasados
## Como desarrollador quiero introducir los datos de mis tareas personales
Introducirá los datos correspondientes a las “tareas personales” que lleva a cabo en el marco de las actividades que tiene asignadas en el proyecto o proyectos en los que está involucrado en cada periodo de tiempo (semana)

AC:

- En cada proyecto, la suma semanal del tiempo de sus actividades no deberá superar a la correspondiente a su porcentaje de particpación en el mismo (40%)
- La suma de todas las actividades de todos los proyectos en los que esté implicado no deberá superar las 40h semanales
- El sistema marcará para cada actividad, el tiempo sobreasignado

## Como desarrollador quiero consultar los datos de las actividades de mis proyectos actuales
Tanto de las actividades activas como las cerradas.

## Como desarrollador quiero acceder a resúmenes de los proyectos finalizados
- En cualquier caso, no se accederá a datos personalizados
- No es necesario que haya participado en ellos para consultarlos.

## Como trabajador quiero rellenar mi calendario personal
La planificación del jefe de proyecto se hará de acuerdo con las restricciones fijadas en el calendario personal (no le puede asignar actividades en épocas de vacaciones):

* Un empleado tiene derecho a 4 semanas de vacaciones que podrá disfrutar en un máximo de 2 periodos de semanas completas.
* Un empleado puede elegir sus fechas de vacaciones en el momento que quiera, pero no puede elegirlas en periodos en los que ya tenga actividades asignadas. Una vez fijado un periodo de vacaciones no se podrá modificar.

## Como desarrollador quiero acceder a mis informes de actividad
Informe de sus actividades por semanas durante un periodo de tiempo determinado (lo puede elegir sobre un calendario), con un indicador si los informas de actividad están aceptados, rechazados o pendientes de aprobación por el correspondiente responsable de proyecto, de cualquiera de los proyectos activos en los que esté implicado. Por supuesto no se puede pedir un informe de actividad de una fecha anterior al comienzo del proyecto o posterior a la actual. Tampoco se pueden pedir informes de actividades que aún no han comenzado.

## Como usuario de la aplicación quiero poder acceder a ella
Implementación de un sistema de log in

### Roles de acceso a la aplicación:
- Administrador del sistema
- Jefe de Proyecto
- Desarrollador

(los roles de JP y DEV pueden solaparse en el caso de usuarios en >1 proyecto distinto)

## Como administrador del sistema quiero configurar la aplicación
Configurar el máximo de proyectos en los que puede participar un desarrollador. Por defecto será 2 proyectos por persona.

## Como jefe de proyecto quiero cerrar actividades y proyectos finalizados
Decidir el instante de finalización de:

* Actividades (cuando se deposite el artefacto correspondiente)
* Etapas (marcadas por un hito)
* Proyectos completos (cuando corresponda)
