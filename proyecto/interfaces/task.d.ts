interface ITask {
	idTarea?: number
	nombre: string
	tipo: taskType
	descripcion: string
	hecho: number
	fecha: Date
	horas: number
	login: string
	idActividad: number
}

declare enum taskType {
	TRATO_USUARIOS,
	REUNIONES,
	LECTURA_REVISION,
	ELABORACION_DOCUMENTACION,
	DESARROLLO_VERIFICACION,
	FORMACION_OTROS
}
