interface IProject {
	id?: number
	nombre: string
	miembros?: IWorker[]
	inicio?: Date
	fin?: Date
	inicioReal?: Date
	finReal?: Date
	plan?: string
	actividades?: IActivity[]
}
