interface IUser {
	login: string
	pass?: string
	nombre: string
	isAdmin: boolean
	proyectos?: IProject[]
	vacaciones?: IHoliday[]
}
