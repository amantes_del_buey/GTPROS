interface IActivity {
	id?: number
	categoria: number
	nombre: string
	descripcion: string
	duracionEstimada: number
	inicio: Date
	fin: Date
	inicioReal?: Date
	finReal?: Date
	tiempoSobreasignado?: number
	artefacto?: string
	idProyecto: number
	predecesoras?: IActivity[]
	// tareas?: ITask[]
}
