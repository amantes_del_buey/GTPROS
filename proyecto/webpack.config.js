var webpack = require('webpack');
var path = require('path');
var fs = require('fs');

var nodeModules = {};
fs.readdirSync('node_modules')
	.filter(function(x) {
		return ['.bin'].indexOf(x) === -1;
	})
	.forEach(function(mod) {
		nodeModules[mod] = 'commonjs ' + mod;
	});

module.exports = [
	{
		resolve: {
			extensions: ['', '.ts', '.webpack.js', '.web.js', '.js']
		},
		target: 'node',
		entry: "./servidor/index",
		output: {
			path: __dirname,
			filename: "server.js"
		},
		externals: nodeModules,
		module: {
			loaders: [
				{test: /\.ts$/, loader: 'ts-loader'},
				{test: /\.json$/, loader: 'json-loader'}
			]
		}
	},
	{
		resolve: {
			extensions: ['', '.ts', '.webpack.js', '.web.js', '.js']
		},
		devtool: 'source-map',
		entry: "./cliente/index.ts",
		output: {
			path: path.join(__dirname, "cliente"),
			filename: "bundle.js"
		},
		module: {
			loaders: [
				{test: /\.css$/, loader: "style!css"},
				{test: /\.jpg$/, loader: "file-loader" },
				{test: /\.html$/, loader: "raw"},
				{test: /\.ts$/, loader: 'ts-loader'}
			]
		}
	}
];
