/**
 * Middleware que se procesa antes de las rutas para restringirlas según los
 * permisos del usuario con sesión activa.
 */
export module Sesion {
	/**
	 * Falla si no existe sesión activa.
	 */
	export function loggedIn(req, res, next) {
		if (req.session.user) {
			return next()
		}

		return res.status(401).end()
	}

	/**
	 * Falla si no existe sesión activa o si la sesión es de un usuario no
	 * administrador.
	 */
	export function isAdmin(req, res, next) {
		if (!req.session.user) {
			return res.status(401).end()
		}

		if (!req.session.user.isAdmin) {
			return res.status(403).end()
		}

		return next()
	}
}
