/// <reference path="./typings/tsd.d.ts"/>
/// <reference path="../interfaces/tsd.d.ts"/>

/// <reference path="./routes/admin"/>

import * as express from 'express'
import * as path from 'path'
import * as session from 'express-session'
import {LoginRoute} from './routes/login'
import {AdminRoute} from './routes/admin'
import {UserRoute} from './routes/user'
import {RolesRoute} from './routes/roles'
import {ProjectRoute} from './routes/project'
import {ActivityRoute} from './routes/activity'
import {TaskRoute} from './routes/task'
import {Sesion} from './middleware'

/**
 * Módulo que carga todas las rutas y arranca el servidor.
 */
module Server {
	let multer = require('multer')
	let bodyParser = require('body-parser')

	let app = express()

	app.use('/', express.static("./cliente"))
	app.use('/uploads', express.static("./uploads"))
	app.use('/documentacion', express.static("./documentacion"))
	app.use('/seguimiento', express.static("./seguimiento.pdf"))
	app.use('/analisis', express.static("./analisis.pdf"))
	app.use(session({
		secret: "Random string",
		cookie: {maxAge: 1000*60*60*24} // La sesión caduca en 24 horas
	}))
	app.use(bodyParser.json())
	const uploadDest = './uploads/'
	const uploadLimits = {
		fileSize: '20971520' // 20MB
	}

	app.post('/login', LoginRoute.login)
	app.post('/logout', LoginRoute.logout)
	app.route('/user')
		.get(Sesion.loggedIn, UserRoute.get)
		.post(Sesion.isAdmin, UserRoute.post)
	app.route('/config')
		.get(Sesion.loggedIn, AdminRoute.getConfig)
		.post(Sesion.isAdmin, AdminRoute.postConfig)
	app.route('/roles')
		.get(Sesion.loggedIn, RolesRoute.get)
		.post(Sesion.isAdmin, multer({
			dest: uploadDest,
			fileFilter: function(req, file, cb) {
				if (file.mimetype == 'text/csv') {
					cb(null, true)
				} else {
					cb(null, false)
				}
			},
			limits: uploadLimits
		}).single('file'), RolesRoute.post)
		.delete(Sesion.isAdmin, RolesRoute.del)
	app.route('/project')
		.get(Sesion.loggedIn, ProjectRoute.get)
		.post(Sesion.isAdmin, ProjectRoute.post)
		.put(Sesion.loggedIn, multer({
			dest: uploadDest,
			fileFilter: function(req, file, cb) {
				if (file.mimetype == 'application/pdf') {
					cb(null, true)
				} else {
					cb(null, false)
				}
			},
			limits: uploadLimits
		}).single('plan'), ProjectRoute.put) // FIXME Debería comprobar que es jefe de proyecto
	app.route('/project/:id')
		.get(Sesion.loggedIn, ProjectRoute.getById)
		.patch(Sesion.loggedIn, ProjectRoute.closeProject)
	app.route('/project/:id/plan')
		.get(Sesion.loggedIn, ProjectRoute.getPlan)
	app.route('/project/:id/activity')
		.get(Sesion.loggedIn, ActivityRoute.get)
	app.route('/activity')
		.post(Sesion.loggedIn, ActivityRoute.post)
	app.route('/activity/:idA')
		.get(Sesion.loggedIn, ActivityRoute.getById)
		.patch(Sesion.loggedIn, ActivityRoute.patchActivity)
	app.route('/activity/:idA/task')
		.get(Sesion.loggedIn, TaskRoute.get)
		.post(Sesion.loggedIn, TaskRoute.post)
		.patch(Sesion.loggedIn, TaskRoute.patch)
	app.route('/holidays')
		.get(Sesion.loggedIn, UserRoute.get_vacaciones)
		.post(Sesion.loggedIn, UserRoute.post_vacaciones)

	let server = app.listen(process.env.PORT || 80, function () {
		let host = server.address().address;
		let port = server.address().port;

		console.log('Servidor escuchando en http://%s:%s', host, port);
	})
}
