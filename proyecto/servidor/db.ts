export module db {
	let knex = require('knex')({
		client: 'mysql',
		connection: require('../mysqlconfig.json')[process.env.ENV || 'dev'],
		debug: true
	})
	let bookshelf = require('bookshelf')(knex)

	export let Vacaciones = bookshelf.Model.extend({
		tableName: 'Vacaciones',
		usuario: function() {
			return this.belongsTo(Usuario, "login")
		}
	})

	export let Trabajador = bookshelf.Model.extend({
		tableName: 'Trabajador',
		usuario: function() {
			return this.belongsTo(Usuario, "login")
		},
		proyecto: function() {
			return this.belongsTo(Proyecto, "idProyecto")
		}
	})

	export let Proyecto = bookshelf.Model.extend({
		tableName: 'Proyecto',
		miembros: function() {
			return this.belongsToMany(Usuario, 'Trabajador', 'idProyecto', 'login').withPivot(['rol'/*'categoria'*/, 'porcentaje'])
		},
		trabajadores: function() {
			return this.hasMany(Trabajador)
		},
		actividades: function() {
			return this.hasMany(Actividad, 'idProyecto')
		}
	})

	export let Usuario = bookshelf.Model.extend({
		tableName: 'Usuario',
		idAttribute: 'login',
		proyectos: function() {
			return this.belongsToMany(Proyecto, 'Trabajador', 'login', 'idProyecto').withPivot(['rol'])
		},
		trabajadores: function() {
			return this.hasMany(Trabajador)
		},
		vacaciones: function() {
			return this.hasMany(Vacaciones,'login')
		},
		tareas: function() {
			return this.hasMany(Tarea, 'login')
		}
	})

	export let Actividad = bookshelf.Model.extend({
		tableName: 'Actividad',
		tareas: function() {
			return this.hasMany(Tarea, 'idActividad')
		},
		predecesoras: function() {
			return this.belongsToMany(Actividad, 'Predecesora', 'idActividad', 'idPredecesora')
		},
		proyecto: function() {
			return this.belongsTo(Proyecto, 'idProyecto')
		}
	})

	export let Tarea = bookshelf.Model.extend({
		tableName: 'Tarea',
		usuario: function() {
			return this.belongsTo(Usuario, 'login')
		},
		proyecto: function() {
			return this.belongsTo(Proyecto, 'idProyecto')
		},
		actividad: function() {
			return this.belongsTo(Actividad, 'idActividad')
		}
	})

	export let Predecesora = bookshelf.Model.extend({
		tableName: 'Predecesora',
		actividad: function() {
			return this.belongsTo(Actividad, 'idActividad')
		},
		predecesora: function() {
			return this.belongsTo(Actividad, 'idPredecesora')
		}
	})

	/**
	 * Función de utilidad para corregir un bug que se produce en el nombre
	 * de las claves relacionadas que devuelve el ORM.
	 */
	export function unpivot(arr) {
		let newArr = []
		arr.forEach(obj => {
			let prefix = '_pivot_'
			Object.keys(obj).forEach(key => {
				if (key.lastIndexOf(prefix, 0) == 0) {
					obj[key.substring(prefix.length)] = obj[key]
					delete obj[key]
				}
			})
			newArr.push(obj)
		})
		return newArr
	}
}
