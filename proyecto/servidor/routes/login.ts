
import {db} from '../db'

/**
 * Función de utilidad que comprueba si la contraseña de un usuario es correcta,
 * o devuelve el usuario solicitado desde la base de datos.
 */
export function getUser(user: IUser, checkPassword?: boolean): Promise<IUser> {
	function serializeUser(userFromDB): IUser {
		// Filter de las vacaciones para extraer, sólo las de este año
		let curr_year: number = new Date().getFullYear()
		return {
			login: userFromDB.get('login'),
			nombre: userFromDB.get('nombre'),
			isAdmin: !!userFromDB.get('isAdmin'),
			proyectos: db.unpivot(userFromDB.related('proyectos').serialize()),
			vacaciones: userFromDB.related('vacaciones').serialize()
				.filter(function(v){return v.fecha.getFullYear() == curr_year})
		}
	}

	return new Promise<IUser>((resolve, reject) => {
		db.Usuario.where('login', user.login).fetch({withRelated: ['proyectos', 'vacaciones'], require: true}).then(userFromDB => {
			if (checkPassword) {
				require("bcrypt-nodejs").compare(user.pass, userFromDB.get('pass'), (err, res) => {
					if (res) {
						resolve(serializeUser(userFromDB))
					} else {
						reject()
					}
				})
			} else {
				resolve(serializeUser(userFromDB))
			}
		}).catch(reject)
	})
}

/**
 * Módulo que gestiona las funciones relacionadas con la sesión del usuario.
 */
export module LoginRoute {
	/**
	 * Permite iniciar una sesión.
	 */
	export function login(req, res) {
		if (req.session.user) {
			// Si ya está logueado
			res.sendStatus(412)
		} else {
			getUser(req.body, true)
				.then(u => {
					req.session.user = u
					res.status(200).json(u)
				})
				.catch(() => res.sendStatus(400))
		}
	}

	/**
	 * Permite finalizar una sesión.
	 */
	export function logout(req, res) {
		if (req.session.user) {
			req.session.user = null
			res.sendStatus(204)
		} else {
			res.sendStatus(412)
		}
	}
}
