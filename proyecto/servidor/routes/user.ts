import {db} from '../db'

/**
 * Rutas del servidor relacionadas con la gestión de usuarios.
 */
export module UserRoute {
	function createUser(u: IUser):Promise<number> {
		return new Promise<number>((resolve, reject) => {
			db.Usuario.where('login', u.login).fetch().then((userFromDB) => {
				if (userFromDB !== null) {
					//El usuario con ese login ya existe en la BD.
					reject(412)
				} else {
					//No existe ese login en la bd, lo creamos.
					db.Usuario.forge(u).save({}, {method: "insert"}).then(() => resolve(201), () => reject(500))
				}
			})
		})
	}

	function createVacaciones(u: IUser, v: IHoliday): Promise<void> {
		return db.Vacaciones.forge(v).save({login:u.login})
	}

	// Devuelve las vacaciones del año presente.
	function retrieveVacaciones(u: IUser): Promise<IHoliday[]> {
		return new Promise<IHoliday[]>((resolve, reject) => {
			db.Vacaciones.where('login', u.login).fetchAll().then(vacaciones => {
				let curr_year: number = new Date().getFullYear()
				resolve(vacaciones.serialize().filter(v => v.fecha.getFullYear() == curr_year))
			}, reject)
		})
	}

	function solapadas(inicioA: Date, semanasA: number, inicioB: Date, semanasB: number): boolean {
		let finB = new Date(inicioB.getTime())
		finB.setDate(finB.getDate() + semanasB*7)

		return solapadasFechas(inicioA, semanasA, inicioB, finB)
	}

	function solapadasFechas(inicioA: Date, semanasA: number, inicioB: Date, finB: Date): boolean {
		let finA = new Date(inicioA.getTime())
		finA.setDate(finA.getDate() + semanasA*7)

		return (inicioA < finB) && (finA > inicioB)
	}

	/**
	 * Obtiene el usuario actual, o todos si se añade el parámetro get `?all=true`.
	 */
	export function get(req, res) {
		if (req.query.all === "true") {
			db.Usuario.fetchAll({withRelated: ['proyectos'], require: true}).then(usuarios => {
				res.send(usuarios.toJSON())
			}).catch(() => {
				res.sendStatus(500)
			})
		} else {
			res.json(req.session.user)
		}
	}

	/**
	 * Crea un nuevo usuario en la aplicación.
	 */
	export function post(req, res){
		let bcrypt = require("bcrypt-nodejs")
		let user: IUser = {
			login: req.body.login,
			pass: bcrypt.hashSync(req.body.pass),
			nombre: req.body.nombre,
			isAdmin: req.body.isAdmin
		}

		createUser(user).then(status => res.sendStatus(status), status => res.sendStatus(status))
	}

	/**
	 * Obtiene las vacaciones para el usuario con sesión activa.
	 */
	export function get_vacaciones(req, res){
		if (req.session.user) {
			retrieveVacaciones(req.session.user).then(v => res.json(v))
		} else {
			res.sendStatus(401)
		}
	}

	/**
	 * Crea un nuevo periodo de vacaciones para el usuario activo.
	 */
	export function post_vacaciones(req, res){
		retrieveVacaciones(req.session.user).then(v => {
			let semanas = parseInt(req.body.semanas)

			if (!semanas) {
				return res.status(400).json({error: `El valor introducido para semanas no es válido.`})
			}

			if (v.length >= 2) {
				return res.status(400).json({error: 'Ya has seleccionado dos periodos de vacaciones en este año. No puedes seleccionar más.'})
			}

			if (new Date(req.body.fecha).getFullYear() != new Date().getFullYear()) {
				return res.status(400).json({code:1 , error:'No se pueden introducir vacaciones en un año diferente al actual'})
			}

			if (req.body.semanas > 4) {
				return res.status(400).json({code:2 , error:'No se puede seleccionar un periodo de vacaciones de más de 4 semanas'})
			}

			if (v.length > 0 && v[0].semanas + req.body.semanas > 4) {
				return res.status(400).json({code:3 , error: `No puedes seleccionar más de 4 semanas de vacaciones. Ya tienes asignadas ${v[0].semanas} semanas.` })
			}

			let vCandidata: IHoliday = {
				fecha: new Date(req.body.fecha),
				semanas: semanas
			}

			if (vCandidata.fecha.getDay() != 1) {
				return res.status(400).json({error: `Asegúrate de seleccionar un lunes como fecha de inicio, pues las vacaciones se asignan por semanas completas.`})
			}

			if (v.length == 1 && solapadas(v[0].fecha, v[0].semanas, vCandidata.fecha, vCandidata.semanas)) {
				return res.status(400).json({code:4, error:'Los periodos vacacionales se solapan entre sí.'})
			}

			db.Usuario.where('login', req.session.user.login).fetch({withRelated: 'tareas'}).then(usuarioConTareas => {
				let actividades = new Set(usuarioConTareas.serialize().tareas.map(t => t.idActividad))
				return db.Actividad.query().whereIn('id', Array.from(actividades)).select().then(a => {
					if (a.some(act => solapadasFechas(vCandidata.fecha, vCandidata.semanas, act.inicio, act.fin))) {
						return Promise.reject("Has intentado seleccionar vacaciones en un periodo en el que hay actividades con tareas que tienes asignadas.")
					}
				})
			}).then(() => {
				createVacaciones(req.session.user, vCandidata).then(() => res.sendStatus(201), () => res.sendStatus(500))
				retrieveVacaciones(req.session.user).then(v => req.session.user.vacaciones = v)
			}, (err) => {
				res.status(400).json({error: err})
			})
		})
	}

}
