import {db} from '../db'
import {getUser} from './login'

/**
 * Rutas para la gestión de proyectos.
 */
export module ProjectRoute {
	function createProject(proyecto: IProject):Promise<number> {
		return new Promise<number>((resolve, reject) => {
			if (!proyecto.miembros) {
				return reject(400)
			}

			db.Proyecto.where('nombre', proyecto.nombre).fetch().then((projectFromDB) => {
				if (projectFromDB !== null) {
					// El nombre de proyecto ya existe.
					return reject(412)
				}

				// No existe este proyecto en la bd, lo creamos.
				let new_proyecto: IProject = {nombre: proyecto.nombre}
				db.Proyecto.forge(new_proyecto).save().then(res => {
					let trabajador: IWorker = {
						login: proyecto.miembros[0].login,
						rol: "JEFE_PROYECTO",
						idProyecto: res.attributes.id,
						porcentaje: 100
					}

					db.Trabajador.forge(trabajador).save().then(() => resolve(201), () => reject(500))
				}, () => reject(500))
			})
		})
	}

	/**
	 * Devuelve todos los proyectos.
	 */
	export function get(req, res) {
		db.Proyecto.fetchAll().then(proyectos => res.json(proyectos))
	}

	export function getPlan(req, res) {
		let idPlan: string
		let nombreProyecto: string
		db.Proyecto.where('id', req.params.id).fetch({require: true}).then(p => {
				p = p.serialize()
				idPlan = p.plan
				nombreProyecto = p.nombre
				if(idPlan === null){
					res.sendStatus(404)
				} else {
					res.download('uploads/'+idPlan,nombreProyecto+'.pdf')
				}
			})
	}

	/**
	 * Devuelve el proyecto solicitado según la ID.
	 */
	export function getById(req, res) {
 		db.Proyecto.where('id', req.params.id)
 			.fetch({withRelated: ['miembros', 'actividades'], require: true})
 			.then(p => {
 				p = p.serialize()
 				p.miembros = db.unpivot(p.miembros)
 				p.actividades = db.unpivot(p.actividades)
 				res.json(p)}
 			)
 	}

	/**
	 * Crea un nuevo proyecto.
	 */
	export function post(req, res) {
		let project: IProject = req.body.project
		createProject(project).then(status => res.sendStatus(status), status => res.sendStatus(status))
	}

	/**
	 * Modifica un proyecto. Usado en la inicialización de proyectos por parte del jefe del proyecto.
	 */
	export function put(req, res) {
		let project: IProject = req.body
		if (req.file) {
			project.plan = req.file.filename
		} else {
			return res.status(400).json({code:3, error:"Tienes que adjuntar un fichero, y debe ser en formato .pdf."})
		}

		if (!project.id) {
			// PUT es una modificación, tiene que referenciar a un proyecto ya existente a través de su id
			return res.status(412).json({code:4, error:"Estás intentando modificar un proyecto que aún no se ha creado."})
		}

		if (!project.miembros) {
			return res.status(400).json({code:5, error:'No puedes guardar un proyecto sin miembros.'})
		}

		if (project.miembros.some(m => !m.login || !m.porcentaje || !m.rol || m.rol == "JEFE_PROYECTO")) {
			return res.status(400).json({code:6, error:'Hay errores en los miembros enviados.'})
		}

		if (req.body.inicio == 'null' || req.body.fin == 'null') {
			return res.status(400).json({code:1 , error:'La planificación del proyecto incluye su fecha de inicio y su fecha de fin.'})
		} else {
			if (project.inicio > project.fin) {
				return res.status(400).json({code:2 , error:'La fecha de inicio no puede ser previa a la fecha de fin.'})
			}
		}

		// Necesario para que MySQL no se queje al intentar meter timestamps en DATEs
		project.inicio = new Date(req.body.inicio)
		project.fin = new Date(req.body.fin)

		// Por seguridad, y para añadir el id de proyecto.
		let miembros = project.miembros.map(m => ({
			login: m.login,
			porcentaje: m.porcentaje,
			rol: m.rol,
			idProyecto: project.id
		}))

		let actividades: IActivity[] = project.actividades.map(m => ({
			nombre: m.nombre,
			descripcion: m.descripcion,
			categoria: m.categoria,
			duracionEstimada: m.duracionEstimada,
			inicio: new Date(m.inicio.toString()),
			fin: new Date(m.fin.toString()),
			predecesoras: m.predecesoras,
			idProyecto: project.id
		}))

		delete project.miembros // Ñapa para evitar que el mysql tenga update de miembros
		delete project.actividades // Ñapa para evitar que el mysql tenga update de actividades


		db.Proyecto.forge(project).save({inicioReal: undefined, finReal: undefined}).then(() => {
			let promises = []
			function checkIfDuplicate(err) {
				if (err.code != 'ER_DUP_ENTRY') {
					return Promise.reject("Error al hacer la petición a la DB.")
				}
			}

			miembros.forEach(m => {
				promises.push(db.Trabajador.forge(m).save().catch(checkIfDuplicate))
			})


			actividades.forEach(a => {
				let predecesorasAct: IActivity[]
				if (a.predecesoras) {
					predecesorasAct = a.predecesoras.map(m => ({
						nombre: m.nombre,
						descripcion: m.descripcion,
						categoria: m.categoria,
						duracionEstimada: m.duracionEstimada,
						inicio: m.inicio,
						fin: m.fin,
						idProyecto: project.id
					}))
				}
				delete a.predecesoras
				promises.push(db.Actividad.forge(a).save().then(res => {
					if (predecesorasAct) {
						a.id = res.attributes.id
						a.predecesoras = predecesorasAct.map(m => ({
							nombre: m.nombre,
							descripcion: m.descripcion,
							categoria: m.categoria,
							duracionEstimada: m.duracionEstimada,
							inicio: m.inicio,
							fin: m.fin,
							idProyecto: project.id
						}))
					} else {
						a.id = res.attributes.id
					}
				}).catch(checkIfDuplicate))
			})

			Promise.all(promises).then(() => {
				actividades.forEach(a => {
					actividades.forEach(b => {
						if (b.predecesoras){
							b.predecesoras.map(m => {
								if (m.nombre === a.nombre
									&& m.descripcion === a.descripcion
									&& m.duracionEstimada === a.duracionEstimada
									&& m.categoria === a.categoria) {
									m.id = a.id
								}
							})
						}
					})
				})

				let promises = []
				actividades.forEach(act => {
					if(act.predecesoras) {
						act.predecesoras.forEach(pred => {
							let nuevaPredecesora = {idActividad: act.id, idPredecesora: pred.id}
							promises.push(db.Predecesora.forge(nuevaPredecesora).save())
						})
					}
				})

				Promise.all(promises).then(() => {
					getUser(req.session.user)
						.then(u => req.session.user = u)
						.then(() => res.sendStatus(200))
				}, () => res.sendStatus(500))
			}, () => res.sendStatus(500))
		}, () => res.sendStatus(500))
	}

	export function closeProject(req, res){
		let idProyecto = req.params.id
		let finR     = req.query.finReal
		db.Proyecto.forge({id:idProyecto}).save({finReal: new Date(finR)})
			.then(() => {
				getUser(req.session.user)
					.then(u => req.session.user = u)
					.then(() => res.sendStatus(200))
			}, () => res.sendStatus(500))
	}
}
