import {db} from '../db'

export module TaskRoute {
	export function get(req, res) {
		db.Tarea.where('idActividad', req.params.idA).fetchAll().then(task => {
			res.send(task.toJSON())
		}, () => {
			res.sendStatus(500)
		})
	}

	export function post(req, res) {
		let task: ITask = req.body
		db.Tarea.forge(task).save().then(() => res.sendStatus(201), () => res.sendStatus(500)).catch(e => console.log(e))
	}

	export function patch(req, res) {
		db.Tarea.forge(req.body.tarea).save().then(() => res.sendStatus(201), () => res.sendStatus(500))
	}
}
