import {db} from '../db'

/**
* Rutas de gestión de las actividades.
*/
export module ActivityRoute {
	/**
	 * Dado un ID de proyecto, obtiene todas las actividades de dicho proyecto.
	 */
	export function get(req, res) {
		db.Actividad.where('idProyecto', req.params.id).fetchAll({withRelated: ['predecesoras', 'tareas']})
			.then(act => {
				res.send(act.toJSON())
			}).catch(() => {
				res.sendStatus(500)
			})
	}

	/**
	 * Devuelve la actividad con el ID solicitado.
	 */
	export function getById(req, res) {
		db.Actividad.where('id', req.params.idA)
			.fetch({withRelated: ['predecesoras', 'tareas']})
			.then(a => res.json(a))
	}

	/**
	 * Crea una nueva actividad.
	 */
	export function post(req, res) {
		let activity: IActivity = req.body.actividad
		db.Actividad.forge(activity).save().then(resp => {
			let id: number = resp.attributes.id
			res.json({id: id}).sendStatus(201)
		}, () => res.sendStatus(500))
	}

	export function patchActivity(req, res) {
		let idActividad = req.params.idA

		let inicioR  = req.query.inicioReal
		let finR     = req.query.finReal

		let artifact: string = req.body.artefacto

		let patchContent

		if(inicioR){
			patchContent = {inicioReal: new Date(inicioR)}
		} else if (finR) {
			patchContent = {finReal: new Date(finR)}
			// TODO: Calcular el tiempo sobreasignado y mandar también
		} else {
			patchContent = {artefacto:artifact}
		}

		db.Actividad.forge({id:idActividad}).save(patchContent)
		.then(() => res.sendStatus(200), () => res.sendStatus(500))
	}
}
