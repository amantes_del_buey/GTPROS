/**
 * Rutas para la gestión de la tabla de roles.
 */
export module RolesRoute {
	let fs = require('fs')
	let csvtojson = require('csvtojson').Converter

	/**
	 * Devuelve la configuración de la aplicación.
	 */
	export function get(req, res) {
		let converter = new csvtojson({})
		let path: string
		try {
			fs.statSync('./roles.custom.csv')
			path = './roles.custom.csv'
		} catch (err) {
			path = './roles.csv'
		}

		try {
			converter.fromFile(path, function(err, result) {
				if (err) {
					res.sendStatus(500)
				} else {
					res.json(result)
				}
			})
		} catch (err) {
			res.sendStatus(500)
		}
	}

	/**
	 * Modifica la configuración de la aplicación.
	 */
	export function post(req, res) {
		let converter = new csvtojson({})
		converter.on("end_parsed", jsonArray => {
			let roles: string[] = []
			let isValid = jsonArray.every(obj => {
				let valid: boolean
				if (Object.keys(obj).length == 2 && obj.rol &&
					obj.categoria && typeof obj.categoria == "number"
					&& obj.rol.length <= 50) {
					roles.push(obj.rol)
					return true
				} else {
					return false
				}
			})

			let rolesUnicos = (new Set(roles)).size === roles.length;

			if (isValid && rolesUnicos) {
				try {
					fs.unlinkSync('./roles.custom.csv')
				} catch (err) {

				}

				try {
					fs.renameSync(req.file.path, './roles.custom.csv')
					res.sendStatus(200)
				} catch (err) {
					res.sendStatus(500)
				}
			} else {
				res.sendStatus(400)
			}
		})

		if (req.file) {
			fs.createReadStream(req.file.path).pipe(converter)
		} else {
			res.sendStatus(400)
		}
	}

	/**
	 * Elimina la configuración de la aplicación, haciendo que se vuelva a
	 * usar la configuración por defecto.
	 */
	export function del(req, res) {
		try {
			fs.unlinkSync('./roles.custom.csv')
			res.sendStatus(200)
		} catch (err) {
			res.sendStatus(412)
		}
	}

}
