/**
 * Ruta para obtener y modificar la configuración de la aplicación.
 */

export module AdminRoute {
		let fs = require('fs');

		/**
		 * Devuelve la configuración de la aplicación.
		 */
		export function getConfig(req, res) {
			let data = fs.readFileSync('servidor/config.json'),
				myObj;

			try {
				myObj = JSON.parse(data);
				res.json(myObj)
			} catch (err) {
				res.sendStatus(500)
			}
		}

		/**
		 * Modifica la configuración de la aplicación.
		 */
		export function postConfig(req, res) {
				let data = JSON.stringify(req.body)
				fs.writeFile('servidor/config.json', data, function (err) {
					if (err)
						res.sendStatus(500)
					res.json(JSON.parse(data))
				})
		}
}
