CREATE DATABASE IF NOT EXISTS pgp CHARACTER SET = "utf8";

USE pgp;
-- USE PGP_grupo07;

DROP TABLE IF EXISTS Predecesora;
DROP TABLE IF EXISTS Tarea;
DROP TABLE IF EXISTS Trabajador;
DROP TABLE IF EXISTS Actividad;
DROP TABLE IF EXISTS Proyecto;
DROP TABLE IF EXISTS Vacaciones;
DROP TABLE IF EXISTS Usuario;

CREATE TABLE Usuario (
	login VARCHAR(30) PRIMARY KEY,
	pass CHAR(60) BINARY, -- bcrypt hash
	nombre CHAR(120),
	isAdmin BOOLEAN
);

INSERT INTO Usuario (login, pass, nombre, isAdmin)
VALUES
	('admin', '$2a$04$b8SMwg2oc3y5u6x.3jP2K.v.pVHTTEDrVcDADmUkcl2/oODcdurtK','Administrador del Sistema',1),
	('user',  '$2a$04$b8SMwg2oc3y5u6x.3jP2K.v.pVHTTEDrVcDADmUkcl2/oODcdurtK','Usuario de la aplicacion',0),
	('adrdela','$2a$04$b8SMwg2oc3y5u6x.3jP2K.v.pVHTTEDrVcDADmUkcl2/oODcdurtK','Adrián de la Rosa',0),
	('jorcuad','$2a$04$b8SMwg2oc3y5u6x.3jP2K.v.pVHTTEDrVcDADmUkcl2/oODcdurtK','Jorge Cuadrado',0),
	('samalfa','$2a$04$b8SMwg2oc3y5u6x.3jP2K.v.pVHTTEDrVcDADmUkcl2/oODcdurtK','Samuel Alfageme',0);


CREATE TABLE Proyecto (
	id INTEGER PRIMARY KEY auto_increment,
	nombre CHAR(130) NOT NULL,
	inicio DATE DEFAULT NULL,
	fin DATE DEFAULT NULL,
	inicioReal DATE DEFAULT NULL,
	finReal DATE DEFAULT NULL,
	plan CHAR(32) DEFAULT NULL,
	CONSTRAINT UNIQUE (nombre)
);

INSERT INTO Proyecto (nombre)
VALUES
	('Creación de una base de datos empresarial');

INSERT INTO Proyecto (nombre, inicio, fin, inicioReal)
VALUES
	('Proyecto activo', '2016-01-11', '2016-05-21', '2016-01-13'),
	('Proyecto activo posterior', '2016-02-11', '2016-06-21', '2016-02-13');

INSERT INTO Proyecto (nombre, inicio, fin, inicioReal, finReal)
VALUES
	('Proyecto cerrado', '2015-01-11', '2015-05-21', '2015-01-13', '2015-07-11');


CREATE TABLE Trabajador (
	idProyecto INTEGER NOT NULL,
	rol VARCHAR(50) NOT NULL,
	login VARCHAR(30) NOT NULL,
	porcentaje INTEGER NOT NULL DEFAULT 100,
	FOREIGN KEY (idProyecto) REFERENCES Proyecto(id),
	FOREIGN KEY (login) REFERENCES Usuario(login),
	CONSTRAINT UNIQUE(idProyecto, rol, login, porcentaje)
);

INSERT INTO Trabajador (idProyecto, rol, login)
VALUES
	(1,'JEFE_PROYECTO','adrdela'),
	(2,'JEFE_PROYECTO','adrdela'),
	(3,'PROBADOR','adrdela'),
	(4,'PROGRAMADOR','adrdela'),
	(1,'ANALISTA','jorcuad'),
	(1,'PROGRAMADOR','samalfa');

CREATE TABLE Vacaciones (
	login VARCHAR(30),
	fecha DATE,
	semanas TINYINT UNSIGNED,
	FOREIGN KEY (login) REFERENCES Usuario(login),
	UNIQUE unique_index (login,fecha)
);

INSERT INTO Vacaciones (login, fecha, semanas)
VALUES
	('adrdela','2016-02-01',3),
	('adrdela','2016-04-04',1),
	('jorcuad','2016-02-01',2),
	('samalfa','2016-02-01',3);

CREATE TABLE Actividad (
	id INTEGER PRIMARY KEY auto_increment,
	nombre VARCHAR(50) NOT NULL,
	descripcion VARCHAR(200) NOT NULL,
	categoria INTEGER NOT NULL,
	inicio DATE NOT NULL,
	fin DATE NOT NULL,
	duracionEstimada INTEGER NOT NULL,
	inicioReal DATE DEFAULT NULL,
	finReal DATE DEFAULT NULL,
	tiempoSobreasignado INTEGER DEFAULT NULL,
	artefacto VARCHAR(200) DEFAULT NULL,
	idProyecto INTEGER NOT NULL,
	FOREIGN KEY (idProyecto) REFERENCES Proyecto(id)
);

INSERT INTO Actividad (nombre, descripcion, categoria, inicio, fin, duracionEstimada, idProyecto)
VALUES
	('Actividad 1', 'Actividad 1', 1, '2016-01-11', '2016-01-16', 2, 2),
	('Actividad 2', 'Actividad 2', 3, '2016-01-11', '2016-01-15', 2, 2),
	('Actividad 3', 'Actividad 3', 5, '2016-01-17', '2016-01-20', 2, 2),
	('Actividad 4', 'Actividad 4', 2, '2016-01-21', '2016-02-15', 2, 2),
	('Actividad 5', 'Actividad 5', 5, '2016-01-21', '2016-01-30', 2, 2),
	('Actividad 1', 'Actividad 1', 2, '2016-03-21', '2016-03-30', 2, 3),
	('Actividad 2', 'Actividad 2', 5, '2016-04-01', '2016-04-30', 2, 3);

INSERT INTO Actividad (nombre, descripcion, categoria, inicio, fin, inicioReal, finReal, tiempoSobreasignado, duracionEstimada, idProyecto)
VALUES
	('Fix de los drop del script de BD', 'Freegan ennui direct trade, everyday carry pinterest echo park heirloom helvetica authentic post-ironic synth mustache locavore. Ethical banjo lomo etsy fixie.', '1', '2015-06-11', '2016-11-16', '2015-06-11', '2016-11-16', 12, 2, 2),
	('Actividad 2', 'Actividad 2', '3', '2015-11-17', '2015-12-15', '2015-11-17', '2015-12-15', 0, 2, 4),
	('Actividad 3', 'Actividad 3', '5', '2015-08-17', '2016-01-13', '2015-08-17', '2016-01-13', 5, 2, 4);

CREATE TABLE Tarea (
	id INTEGER PRIMARY KEY auto_increment,
	nombre VARCHAR(50) NOT NULL,
	descripcion VARCHAR(200) NOT NULL,
	tipo INTEGER NOT NULL,
	login VARCHAR(30) DEFAULT NULL,
	fecha DATE, -- NOT NULL,
	hecho BOOLEAN, -- NOT NULL,
	horas INTEGER DEFAULT NULL,
	idActividad INTEGER NOT NULL,
	FOREIGN KEY (idActividad) REFERENCES Actividad(id),
	FOREIGN KEY (login) REFERENCES Usuario(login)
);

INSERT INTO Tarea (nombre, descripcion, tipo, login, idActividad, hecho, fecha, horas)
VALUES
	('Tarea 1', 'Tarea 1', 1, 'jorcuad', 1, 0, '2016-01-16', 1),
	('Tarea 2', 'Tarea 2', 1, 'adrdela', 2, 0, '2016-01-16', 12),
	('Tarea 3', 'Tarea 3', 1, 'samalfa', 2, 0, '2016-01-16', 3),
	('Tarea 4', 'Tarea 4', 1, 'samalfa', 2, 0, '2016-01-16', 7),
	('Tarea 5', 'Tarea 5', 1, 'adrdela', 4, 0, '2016-01-16', 7);

CREATE TABLE Predecesora (
	idActividad INTEGER NOT NULL,
	idPredecesora INTEGER NOT NULL,
	FOREIGN KEY (idActividad) REFERENCES Actividad(id),
	FOREIGN KEY (idPredecesora) REFERENCES Actividad(id),
	CONSTRAINT UNIQUE(idActividad, idPredecesora)
);

INSERT INTO Predecesora (idActividad, idPredecesora)
VALUES
	(9,8),
	(9,10),
	(3,1),
	(3,2),
	(4,3),
	(5,3),
	(7,6);
