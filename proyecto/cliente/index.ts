/// <reference path="./typings/tsd.d.ts"/>
/// <reference path="../interfaces/tsd.d.ts"/>

import './estilos/angular-material.min.css'
import './estilos/breadcrumb.css'
import './estilos/mdl-components.css'
import './estilos/custom.css'

import * as angular from 'angular'
import './angular-locale_es-es.js'
import 'angular-ui-router'
import 'angular-aria'
import 'angular-animate'
import 'angular-material'
import 'angular-breadcrumb'
require('../node_modules/ng-file-upload/dist/ng-file-upload.js')

import {LoginCtrl} from './controladores/login'
import {MainCtrl} from './controladores/main'
import {AppCtrl} from './controladores/app'
import {AdminCtrl} from './controladores/admin'
import {UserCtrl} from './controladores/user'
import {CreateUserCtrl} from './controladores/admin/create_user'
import {CreateProjectCtrl} from './controladores/admin/create_project'
import {ProjectCtrl} from './controladores/user/project'
import {InitProjectCtrl} from './controladores/user/init-project';
import {RolesCtrl} from './controladores/admin/roles'
import {CalendarCtrl} from './controladores/calendar'
import {ActivityCtrl} from './controladores/activity'

import {sesion} from './servicios/sesion'

/**
 * Configura la aplicación cliente y la arranca.
 */
module Client {
	angular.module('gtpros', ['ui.router', 'ngMaterial', 'ncy-angular-breadcrumb', 'ngFileUpload'])

	.service('sesion', sesion)

	.config(function($stateProvider, $urlRouterProvider) {
		$urlRouterProvider.when(/app/i, function(sesion, $mdToast): any {
			if (!sesion.syncUsuario) {
				$mdToast.showSimple("Tienes que iniciar sesión.")
				return 'login'
			} else {
				return false
			}
		})

		$urlRouterProvider.when('', function(sesion) {
			if (!sesion.syncUsuario) {
				return 'login'
			} else if (sesion.syncUsuario.isAdmin) {
				return 'app/admin/main'
			} else {
				return 'app/user/projects'
			}
		})

		$urlRouterProvider.when(/app\/user/i, function(sesion, $mdToast): any {
			if (!sesion.syncUsuario.isAdmin) {
				return false
			} else {
				$mdToast.showSimple("Has accedido como administrador así que no puedes ir a la parte de usuario.")
				return "login"
			}
		})

		$urlRouterProvider.when(/app\/admin/i, function(sesion, $mdToast): any {
			if (sesion.syncUsuario.isAdmin) {
				return false
			} else {
				$mdToast.showSimple("Has accedido como usuario así que no puedes ir a la parte de admin.")
				return "login"
			}
		})

		$stateProvider
		.state('login', {
			url: '/login',
			template: require('./vistas/login.html'),
			controller: 'LoginCtrl',
			controllerAs: 'login'
		})
		.state('app', {
			url: '/app',
			abstract: true,
			template: require('./vistas/app.html'),
			controller: 'AppCtrl',
			controllerAs: 'app',
			ncyBreadcrumb: {
				skip: true
			}
		})
			.state('app.admin', {
				url: '/admin',
				abstract: true,
				controller: 'AdminCtrl',
				controllerAs: 'admin',
				template: `<ui-view />`,
				ncyBreadcrumb: {
					skip: true
				}
			})
				.state('app.admin.main', {
					url: '/main',
					template: require('./vistas/admin/admin.html'),
					ncyBreadcrumb: {
						label: "Administración"
					}
				})
				.state('app.admin.createUser', {
					url: '/create_user',
					template: require('./vistas/admin/create_user.html'),
					controller: 'CreateUserCtrl',
					controllerAs: 'createUser',
					ncyBreadcrumb: {
						parent: 'app.admin.main',
						label: "Crear usuario"
					}
				})
				.state('app.admin.createProject', {
					url: '/create_project',
					template: require('./vistas/admin/create_project.html'),
					controller: 'CreateProjectCtrl',
					controllerAs: 'createProject',
					ncyBreadcrumb: {
						parent: 'app.admin.main',
						label: "Crear proyecto"
					}
				})
				.state('app.admin.config', {
					url: '/config',
					template: require('./vistas/admin/config.html'),
					ncyBreadcrumb: {
						parent: 'app.admin.main',
						label: "Configuración"
					}
				})
				.state('app.admin.roles', {
					url: '/roles',
					template: require('./vistas/admin/roles.html'),
					controller: 'RolesCtrl',
					controllerAs: 'roles',
					ncyBreadcrumb: {
						parent: 'app.admin.main',
						label: 'Tabla de roles'
					}
				})
			.state('app.user', {
				url: '/user',
				abstract: true,
				template: `<ui-view />`,
				controller: 'UserCtrl',
				controllerAs: 'user',
				ncyBreadcrumb: {
					skip: true
				}
			})
				.state('app.user.calendar', {
					url: '/calendar',
					template: require('./vistas/calendar.html'),
					controller: 'CalendarCtrl',
					controllerAs: 'calendar',
					ncyBreadcrumb: {
						parent: "app.user.projects",
						label: "Calendario Personal"
					}
				})
				.state('app.user.projects', {
					url: '/projects',
					template: require('./vistas/user/user.html'),
					ncyBreadcrumb: {
						label: "Proyectos"
					}
				})
				.state('app.user.project', {
					url: '/project/:id',
					template: require('./vistas/user/project.html'),
					controller: 'ProjectCtrl',
					controllerAs: 'project',
					resolve: {
						project: function($http, $stateParams) {
							let id = $stateParams.id
							return $http.get('/project/' + id).then(res => res.data)
						}
					},
					ncyBreadcrumb: {
						parent: "app.user.projects",
						label: "Proyecto {{project.project.id}}"
					}
				})
					.state('app.user.project.init', {
						url: '/init',
						template: require('./vistas/user/crear-proyecto-jp.html'),
						controller: 'InitProjectCtrl',
						controllerAs: 'initProject',
						resolve: {
							proyecto: function($http, $stateParams) {
								let id = $stateParams.id
								return $http.get('/project/' + id).then(res => res.data)
							},
							usuarios: function($http) {
								return $http.get('/user?all=true').then(res => res.data)
							},
							roles: function($http) {
								return $http.get('/roles').then(res => res.data)
							}
						},
						ncyBreadcrumb: {
							label: 'Configuración inicial'
						}
					})
					.state('app.user.project.activity', {
						url: '/activity/:idA',
						template: require('./vistas/activity.html'),
						controller: 'ActivityCtrl',
						controllerAs: 'activity',
						resolve: {
							activity: function($http, $stateParams) {
								let idA = $stateParams.idA
								return $http.get('/activity/' + idA).then(res => res.data)
							}
						},
						ncyBreadcrumb: {
							parent: "app.user.project",
							label: "Actividad {{activity.activity.idA}}"
						}
					})
		$urlRouterProvider.otherwise('login')
	})

	.controller('MainCtrl', MainCtrl)
	.controller('LoginCtrl', LoginCtrl)
	.controller('AppCtrl', AppCtrl)
	.controller('AdminCtrl', AdminCtrl)
	.controller('UserCtrl', UserCtrl)
	.controller('CreateUserCtrl', CreateUserCtrl)
	.controller('CreateProjectCtrl', CreateProjectCtrl)
	.controller('ProjectCtrl', ProjectCtrl)
	.controller('CalendarCtrl', CalendarCtrl)
	.controller('InitProjectCtrl', InitProjectCtrl)
	.controller('RolesCtrl', RolesCtrl)
	.controller('ActivityCtrl', ActivityCtrl)
	.controller('RolesCtrl', RolesCtrl)

	.directive('listaActividades', function() {
		return {
			scope: {},
			bindToController: {
				actividades: '='
			},
			controller() {},
			controllerAs: 'listaActividades',
			template: require('./vistas/lista-actividades.html')
		}
	})

	.directive('tarjetaProyecto', function() {
		return {
			scope: {},
			bindToController: {
				proyecto: '='
			},
			controller() {},
			controllerAs: 'tarjetaProyecto',
			template: require('./vistas/tarjeta-proyecto.html')
		}
	})

	.config(function($mdDateLocaleProvider) {
		$mdDateLocaleProvider.firstDayOfWeek = 1
	})

}
