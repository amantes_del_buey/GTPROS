export class sesion {
	private _usuario: IUser
	constructor(private $http) {
		$http.get('/user').then(res => {
			this._usuario = res.data
		})
	}

	get syncUsuario() {
		return this._usuario
	}

	usuario() {
		return this.$http.get('/user').then(res => {
			this._usuario = res.data
			return res.data
		})
	}

	login(login: string, pass: string) {
		return this.$http.post('/login', {login: login, pass: pass})
			.then(res => {
				return this._usuario = res.data
			}, res => {
				return Promise.reject(res.status)
			})
	}

	logout() {
		if (!this.usuario) {
			return Promise.reject("Error. No tenías sesión abierta.")
		}

		return this.$http.post('/logout').then(() => {
			this._usuario = undefined
			return 'Sesión cerrada correctamente'
		}, () => {
			this._usuario = undefined
			return 'La sesión se cerró inesperadamente'
		})
	}
}
