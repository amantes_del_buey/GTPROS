/**
 * Controlador principal de la parte de administración de la aplicación.
 */

export class AdminCtrl {
	$inject = ['$state', '$http', '$mdToast']
	constructor(private $state, private $http, private $mdToast) {
		$http.get("/config").then(res => this.max_projects = res.data.MAX_PROJECTS)
	}

	max_projects: number

	private alerta(mensaje) {
		this.$mdToast.showSimple(mensaje)
	}

	setConfig() {
		this.$http.post('/config', {MAX_PROJECTS: this.max_projects}).then((res) => {
			this.$state.go('app.admin.main')
			this.alerta("Se ha modificado la configuración de la aplicación")
		}, response => {
			this.alerta("No se pudo realizar el cambio en la configuración")
		})
	}
}
