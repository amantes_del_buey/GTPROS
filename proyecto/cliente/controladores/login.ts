/**
 * Controlador de la página de acceso a la aplicación.
 */

export class LoginCtrl {
	$inject = ['$state', '$mdToast', 'sesion']
	constructor(private $state, private $mdToast, private sesion) {
		sesion.usuario().then(u => this.usuario = u)
		document.title = "Bienvenido a GTPROS"
	}

	usuario: IUser
	login: string
	pass: string

	private alerta(mensaje) {
		this.$mdToast.showSimple(mensaje)
	}

	doLogin() {
		this.sesion.login(this.login, this.pass).then(u => {
			if (u.isAdmin) {
				this.$state.go('app.admin.main')
			} else {
				this.$state.go('app.user.projects')
			}
		}, status => {
			this.alerta("Error: " + status)
		})
	}

	logout() {
		this.sesion.logout().then(() => this.$state.go('login', {}, {reload: true}))
	}

	start() {
		if (this.usuario.isAdmin) {
			this.$state.go('app.admin.main')
		} else {
			this.$state.go('app.user.projects')
		}
	}
}
