/**
 * Controlador principal de toda la aplicación.
 */

export class AppCtrl {
	$inject = ['$state', 'sesion', '$mdToast']
	constructor(private $state, private sesion, private $mdToast, $rootScope, $interpolate) {
		sesion.usuario().then(u => this.user = u)

		if (document.title == "Bienvenido a GTPROS") {
			document.title = "Proyectos | GTPROS"
		}
		$rootScope.$on('$stateChangeSuccess', function(event, toState) {
			if (toState.ncyBreadcrumb && toState.ncyBreadcrumb.label) {
				let label = toState.ncyBreadcrumb.label
				let int = $interpolate(label)
				label = int(this)
				document.title = `${label} | GTPROS`
			}
		})
	}

	user : IUser

	private alerta(msg: string) {
		this.$mdToast.showSimple(msg)
	}

	logout() {
		this.sesion.logout().then(() => this.alerta, () => this.alerta).then(() => {
			this.$state.go('login')
		})
	}
}
