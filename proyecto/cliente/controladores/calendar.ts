/**
 * Controlador para la gestión de las vacaciones de los empleados.
 */

export class CalendarCtrl {
	$inject = ['$state', '$http', '$mdToast']

	constructor(private $state, private $http, private $mdToast) {
		$http.get("/holidays").then(res => this.vacaciones = res.data)
	}

	vacaciones: IHoliday[]

	inicio: Date = new Date()
	semanas: number

	private alerta(mensaje) {
		this.$mdToast.showSimple(mensaje)
	}

	addVacaciones() {
		this.$http.post('/holidays', {fecha: this.inicio, semanas: this.semanas}).then((res) => {
			this.$state.go('app.user.projects')
			this.alerta("Vacaciones creadas correctamente")
		}, response => {
			this.alerta("Error: " + response.data.error)
		})
	}

	soloLunes(date) {
		return date.getDay() === 1
	}

}
