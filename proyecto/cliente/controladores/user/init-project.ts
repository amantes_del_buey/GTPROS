/**
 * Controlador de la interfaz de inicialización de proyectos.
 */

export class InitProjectCtrl {
	constructor(usuarios, private $state, private $stateParams, private Upload, private $mdToast, proyecto, roles) {
		this.fechaActual = new Date()
		this.users = usuarios
		this.proyecto = proyecto
		let jefeIndex: number
		for (let i = 0; i < this.proyecto.miembros.length; i++) {
			if (this.proyecto.miembros[i].rol == "JEFE_PROYECTO") {
				jefeIndex = i
				break
			}
		}
		this.jefe = this.proyecto.miembros[jefeIndex]
		this.proyecto.miembros.splice(jefeIndex, 1)
		this.roles = roles.filter(r => r.categoria != 1)

		if (!this.proyecto.miembros) {
			this.proyecto.miembros = []
		}

		this.moment = require('moment')

	}

	private moment

	fechaActual: Date

	users: IUser[]
	proyecto: IProject
	jefe: IWorker
	roles: string[]

	alerta(msg) {
		this.$mdToast.showSimple(msg)
	}

	transformChip($chip) {
		return {
			login: $chip.login,
			nombre: $chip.nombre
		}
	}

	querySearch(query) {
		function filter(user) {
			return (user.nombre.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
				(user.login.toLowerCase().indexOf(query.toLowerCase()) > -1)
		}
		return query ? this.users.filter(filter) : []
	}

	back() {
		this.$state.go('app.user.projects')
	}

	save() {
		/*console.log(this.proyecto)
		this.proyecto.actividades.forEach( a => {
			if(a.predecesoras){
				a.predecesoras.map( pred => {
					if(a.predecesoras) delete pred.predecesoras
				})
			}
		})*/
		//console.log(this.proyecto)
		let upload = this.Upload.upload({
			url: `/project`,
			data: this.proyecto,
			method: "PUT"
		})

		upload.then(res => {
			this.alerta("Proyecto correctamente configurado")
			this.$state.go('^', {id: this.$stateParams.id}, {reload: true})
		}, res => {
			this.alerta("Hay algún error en la configuración inicial: " + res.data.error)
		})
	}

	actividadSeleccionada: IActivity
	nuevaActividad: IActivity

	seleccionarActividad(act) {
		this.actividadSeleccionada = act
	}

	addActividad() {
		if(this.checkCompleted()){
			this.setFechasActividad()
			if (this.nuevaActividad.fin > this.proyecto.fin) {
				let maxdays: number = this.moment(this.proyecto.fin).diff(this.nuevaActividad.inicio,'days')
				this.alerta("La duración máxima de esta actividad debe ser "+ maxdays +" para cumplir con el fin de proyecto. Amplie el plazo o modifique las actividades precedentes en caso de haberlas.")
			} else {
				this.proyecto.actividades.push(this.nuevaActividad)
				this.seleccionarActividad(this.nuevaActividad)
				this.nuevaActividad = undefined
			}
		} else {
			this.alerta("Error: Debes completar todos los campos para crear una actividad.")
		}
	}

	checkCompleted() {
		if(this.nuevaActividad === undefined || !this.nuevaActividad.nombre || !this.nuevaActividad.descripcion
			|| this.nuevaActividad.duracionEstimada === undefined  || !this.nuevaActividad.categoria){
				return false;
		} else {
			return true;
		}
	}

	setFechasActividad() {
		let inicioActividad: Date
		if (this.nuevaActividad.predecesoras && this.nuevaActividad.predecesoras.length > 0) {
			inicioActividad = this.nuevaActividad.predecesoras.reduce((x,y) => x.fin > y.fin ? x : y).fin
		}

		if (inicioActividad === undefined) {
			this.nuevaActividad.inicio = this.proyecto.inicio
		} else {
			this.nuevaActividad.inicio = inicioActividad
		}

		this.nuevaActividad.fin = this.moment(this.nuevaActividad.inicio)
			.add(this.nuevaActividad.duracionEstimada, "days").toDate()
	}

	private setFechas(actividad: IActivity): boolean {
		let inicioActividad: Date
		if (actividad.predecesoras && actividad.predecesoras.length > 0) {
			inicioActividad = actividad.predecesoras.reduce((x,y) => x.fin > y.fin ? x : y).fin
		}

		if (inicioActividad === undefined) {
			actividad.inicio = this.proyecto.inicio
		} else {
			actividad.inicio = inicioActividad
		}

		actividad.fin = this.moment(actividad.inicio)
			.add(actividad.duracionEstimada, "days").toDate()

		if (actividad.fin <= this.proyecto.fin) {
			return true
		}
	}

	sinErrores: Boolean

	updateFechasActividad() {
		let copia = angular.copy(this.proyecto.actividades)
		this.sinErrores = copia.every(act => {
			return this.setFechas(act)
		})

		if (!this.sinErrores) {
			this.alerta("La nueva fecha no permite calendarizar correctamente las actividades.")
		} else {
			this.proyecto.actividades.forEach( act => {
				this.setFechas(act)
			})
		}
	}

	toggle(item) {
		if (!this.nuevaActividad.predecesoras) this.nuevaActividad.predecesoras = []
		var idx = this.nuevaActividad.predecesoras.indexOf(item);
		if (idx > -1) this.nuevaActividad.predecesoras.splice(idx, 1);
		else this.nuevaActividad.predecesoras.push(item);
	};

	exists(item) {
		if (!this.nuevaActividad.predecesoras) this.nuevaActividad.predecesoras = []
		return this.nuevaActividad.predecesoras.indexOf(item) > -1;
	};
}
