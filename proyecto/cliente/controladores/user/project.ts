/**
 * Controlador de la vista de detalle de proyecto.
 */

export class ProjectCtrl {
	project: IProject

	fechaActual: Date = new Date()

	activas : IActivity[] = []
	cerradas: IActivity[] = []

	constructor(project, private $state, private $http, private $mdToast) {
		this.project = project

		this.project.actividades.forEach(actividad => {
			if (actividad.finReal) {
				this.cerradas.push(actividad)
			} else {
				this.activas.push(actividad)
			}
		})

		if (!project.inicio) {
			$state.go('.init')
		}
	}

	private alerta(mensaje) {
		this.$mdToast.showSimple(mensaje)
	}

	closeProject(){
		this.$http.patch('/project/'+this.project.id, null, {params:{finReal:this.fechaActual}}).then((res) => {
				// TODO: actualizar sesión
				this.project.finReal = this.fechaActual
				this.alerta("Proyecto cerrado")
			}, response => {
				this.alerta("Error: " + response.status)
		})
	}
}
