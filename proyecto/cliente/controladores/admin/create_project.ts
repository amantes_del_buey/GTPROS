/**
 * Controlador de la interfaz de creación de proyectos..
 */

export class CreateProjectCtrl {
	$inject = ['$state', '$http', '$mdToast']
	constructor(private $state, private $http, private $mdToast) {
		this.getUsers().then(users => this.jefesProyecto = users, error => {
			this.alerta("Error: " + error.status)
		})
		this.$http.get('/config').then(res => {
			this.max_projects = res.data.MAX_PROJECTS
		})
	}

	jefe: string
	projectName: string
	jefesProyecto: IUser[]
	max_projects: number

	private alerta(mensaje) {
		this.$mdToast.showSimple(mensaje)
	}

	canBeJP(item) {
		let num_proyectos : number = item.proyectos.filter(p => p.inicio && !p.finReal && p._pivot_rol =='JEFE_PROYECTO').length
		return num_proyectos < this.max_projects;
	}

	getUsers() {
		return this.$http.get('/user?all=true').then(res => {
			let users = []
			res.data.forEach(obj => {
				if(obj.isAdmin === 0) users.push(obj)
			})
			return users
		})
	}

	crear() {
		if (this.jefe === undefined) {
			return this.alerta("Debe seleccionar un jefe de proyecto.")
		}
		if (this.projectName === undefined) {
			return this.alerta("Debe introducir el nombre del proyecto.")
		}

		let proyecto: IProject = {
			nombre: this.projectName,
			miembros: [{
				porcentaje: 100,
				login: this.jefe
			}]
		}

		this.$http.post('/project', {project: proyecto}).then(res => {
			this.$state.go('app.admin.main')
			this.alerta("Proyecto creado correctamente.")
		}, error => {
			this.alerta("Error: " + error.status)
		})
	}
}
