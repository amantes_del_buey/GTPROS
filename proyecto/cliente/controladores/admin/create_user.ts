/**
 * Controlador de la interfaz de creación de usuarios.
 */

export class CreateUserCtrl {
	$inject = ['$state', '$http', '$mdToast']
	constructor(private $state, private $http, private $mdToast) {}

	nombre: string
	login: string
	pass: string
	passRepeat: string

	private alerta(mensaje) {
		this.$mdToast.showSimple(mensaje)
	}

	create() {
		if(this.pass === this.passRepeat) {
			this.$http.post('/user', {login: this.login, pass: this.pass, nombre: this.nombre, isAdmin: false}).then((res) => {
				this.$state.go('app.admin.main')
				this.alerta("Usuario creado correctamente.")
			}, response => {
				this.alerta("Error: " + response.status)
			})
		} else {
			this.alerta("3Error: Las contraseñas no coinciden.");
		}
	}
}
