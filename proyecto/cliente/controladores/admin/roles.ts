/**
 * Controlador de la interfaz de gestión de la tabla de roles.
 */

export class RolesCtrl {
	constructor(private $http, private $mdToast, private Upload) {

	}

	alerta(msg) {
		this.$mdToast.showSimple(msg)
	}

	borrar() {
		this.$http.delete('/roles').then(
			() => this.alerta("Se ha revertido a la tabla por defecto correctamente."),
			() => this.alerta("No se ha podido revertir a la tabla por defecto.")
		)
	}

	cargar(fichero) {
		if (fichero) {
			this.Upload.upload({
				url: '/roles',
				data: {
					file: fichero
				}
			}).then(res => {
				this.alerta("Tabla modificada correctamente.")
			}, res => {
				this.alerta("No se pudo modificar la tabla.")
			})
		}

	}
}
