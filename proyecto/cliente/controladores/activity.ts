/**
 * Controlador de la gestión de las actividades.
 */

export class ActivityCtrl {
	$inject = ['$state','$http', '$mdToast','sesion']
	activity: IActivity
	newTarea: ITask
	tareas: ITask[]
	tasksFinished: boolean

	fechaActual: Date = new Date()

	constructor(activity, private $state, private $http, private $mdToast, private sesion) {
		this.activity = activity
		/* TODO: cuando se actualice el modelo de tarea & actividad
		this.tasksFinished = activity.tareas
			.reduce((a,b) => a.completada && b.completada)
		*/
		$http.get(`/activity/${this.activity.id}/task`).then(res => {
			this.tareas = res.data
		})
		this.tasksFinished = true
	}

	private alerta(mensaje) {
		this.$mdToast.showSimple(mensaje)
	}


	depositArtifact(){
		this.$http.patch('/activity/'+this.activity.id, {"artefacto":this.activity.artefacto}).then(() => {
				this.alerta("Artefacto depositado correctamente.")
			}, response => {
				this.alerta("Error: " + response.status)
		})
	}

	setInitDate(){
		this.$http.patch('/activity/'+this.activity.id, null, {params:{inicioReal:this.fechaActual}}).then((res) => {
				// Workaround para que no se siga mostrando el botón.
				// Estaría mejor obtenerlo del server
				this.activity.inicioReal = this.fechaActual
				this.alerta("Fecha de inicio real fijada.")
			}, response => {
				this.alerta("Error: " + response.status)
		})
	}

	setEndDate(){
		// FIXME: Ñapa muy fea para evitar que no se persista el artefacto
		this.depositArtifact()
		// TODO: Comprobar todas las restricciones para settear una fecha de fin
		this.$http.patch('/activity/'+this.activity.id, null, {params:{finReal:this.fechaActual}}).then((res) => {
				this.activity.finReal = this.fechaActual
				this.alerta("Fecha de fin real fijada.")
			}, response => {
				this.alerta("Error: " + response.status)
		})
	}

	canBeStarted(){
		// Puede iniciarse cuando todas las predecesoras han sido completadas y no tiene inicio real
		let predecessorsFinished: boolean = this.activity.predecesoras
			.map(p => !!p.finReal)
			.reduce((a,b) => a && b, true)
		return !this.activity.inicioReal && predecessorsFinished
	}

	canBeFinished(){
		// Puede terminarse cuando las tareas se hayan terminado y falte un artefacto por depositar
		return this.tasksFinished && this.activity.inicioReal
	}

	canBeClosed(){
		// Puede cerrarse cuando todas sus tareas se hayan terminado y se haya depositado un artefacto
		return this.canBeFinished() && this.activity.artefacto && !this.activity.finReal
	}

	addTarea() {
		if(this.newTarea.nombre && this.newTarea.descripcion && this.newTarea.tipo) {
			if(this.newTarea.horas) this.newTarea.fecha = new Date()
			console.log(this.sesion.usuario)
			this.$http.post('/activity/'+this.activity.id+'/task',
				{"nombre": this.newTarea.nombre,
				 "descripcion" : this.newTarea.descripcion,
				 "tipo" : this.newTarea.tipo,
				 "hecho" : 0,
				 "idActividad": this.activity.id}
				).then((res) => {
				this.tareas.push(this.newTarea)
				this.newTarea = undefined
				this.alerta("Tarea añadida.")
			}, response => {
				this.alerta("Error: " + response.status)
			})
		} else {
			this.alerta("Debes completar al menos nombre, descripción y tipo para crear una tarea.")
		}
	}

	cerrarTarea(tarea: ITask) {
		tarea.hecho = 1
		this.$http.patch(`/activity/${this.activity.id}/task`, {tarea: tarea}).catch(notUpdated => {
			this.alerta("La tarea no ha podido cerrarse.")
		})
	}

	updateHours(tarea: ITask) {
		if(tarea.horas === 1 && !tarea.fecha) tarea.fecha = new Date()
		console.log(tarea.fecha)
		this.$http.patch(`/activity/${this.activity.id}/task`, {tarea: tarea}).catch(notUpdated => {
			this.alerta("La tarea no ha podido cerrarse.")
		})
	}

	parseTipo(tipo : number) {
		switch(tipo) {
			case 1:
			return "Trato con usuarios"
			case 2:
			return "Reuniones"
			case 3:
			return "Lectura y revisión de documentación"
			case 4:
			return "Elaboración de documentación"
			case 5:
			return "Desarrollo y verificación de programas"
			case 6:
			return "Formación y otros"
		}
	}

}
