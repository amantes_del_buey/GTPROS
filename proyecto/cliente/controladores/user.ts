/**
 * Controlador de la parte de usuario de la aplicación.
 */

export class UserCtrl {
	user: IUser
	proyectosActivos: IProject[] = []
	proyectosNoIniciados: IProject[] = []
	proyectosAntiguos: IProject[] = []

	private alerta(mensaje: string) {
		this.$mdToast.showSimple(mensaje)
	}

	constructor(sesion, private $mdToast, $rootScope) {
		sesion.usuario().then(u => {
			this.user = u

			this.user.proyectos.forEach(proyecto => {
				if (proyecto.finReal) {
					this.proyectosAntiguos.push(proyecto)
				} else if (proyecto.inicioReal){
					this.proyectosActivos.push(proyecto)
				} else {
					this.proyectosNoIniciados.push(proyecto)
				}
			})
		})

		$rootScope.$on('$stateChangeSuccess', function(event, toState) {
			sesion.usuario().then(u => {
				this.user = u

				this.user.proyectos.forEach(proyecto => {
					if (proyecto.finReal) {
						this.proyectosAntiguos.push(proyecto)
					} else if (proyecto.inicioReal){
						this.proyectosActivos.push(proyecto)
					} else {
						this.proyectosNoIniciados.push(proyecto)
					}
				})
			})
		})
	}
}
