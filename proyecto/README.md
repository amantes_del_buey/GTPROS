# GTPROS
## Cómo ejecutarlo
Prerrequisitos: nodejs y `npm install -g webpack`.

1. Estando en la carpeta `proyecto`, ejecutar `npm install`.
2. Después, ejecutar `webpack`. Se compilan todos los ficheros necesarios.
3. Para lanzar el servidor, ejecutar `node server.js`. Se arranca el servidor en el puerto 80 por omisión.

## Ejecución recomendada para desarrollo
Prerrequisitos: Además de los de la sección anterior, `npm install -g nodemon`.

1. Abrir tres terminales en la carpeta `proyecto`.
2. En la primera, ejecutar `webpack --watch`. Se arranca un proceso de webpack que recompilará todo cuando cualquier fichero cambie.
3. En la segunda, ejecutar `PORT=3000 nodemon server.js` que arrancará el servidor en un puerto no reservado. Cuando los ficheros se recompilen, el servidor se reiniciará automáticamente.
4. La tercera terminal será la que se use para trabajar en el proyecto, dejando las otras dos en segundo plano.

## Ejecución rápida
Estando en la carpeta `proyecto`, se puede ejecutar `npm start` para lanzar webpack y nodemon en el puerto 3000 automáticamente. Ambos procesos se cerrarán al pulsar `ctrl+C`.
