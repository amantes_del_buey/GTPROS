% Proyecto GTPROS: Documento de seguimiento
% Jorge Cuadrado Saez; Samuel Alfageme Sanz; Adrián de la Rosa Martín
% 21 de enero de 2016

# Sprint 1: 09 de Noviembre - 22 de Noviembre 2015

## US59 Como usuario, quiero poder ver un breadcrumb para saber dónde estoy y cómo volver a una vista anterior.
### Puntos
- UX: -
- Design: -
- Front: 3
- Back: -

### Tareas
- T64 Añadir y configurar modulo de breadcrumbs
- T65 Poner nombres a los estados
- T66 Configurar los estilos del breadcrumb

## US12 Como jefe de proyecto quiero poder consultar los datos de mis proyectos en curso y pasados.
### Puntos
- UX: 1
- Design: -
- Front: 2
- Back: 1

### Tareas
- T58 Listar todos los proyectos en 2 categorias: en curso y finalizados.

## US82 Como cliente quiero que el código esté comentado
### Puntos

- UX: -
- Design: -
- Front: -
- Back: 5

### Comentarios
Esta user story se planificó ya desde el primer sprint y se ha ido arrastrando hasta el último sprint como actividad transversal al proyecto que es. Se ha decidido representar en su inicio para el documento de seguimiento.

# Sprint 2: 23 de Noviembre - 06 de Diciembre 2015

## US01 Como administrador de sistema quiero dar de alta proyectos
### Descripción:
Asignar Responsable del Proyecto (sólo de un proyecto a la vez, podrá participar como DEV en otros proyectos)

### Puntos
- UX: -
- Design: 1/2
- Front: 1/2
- Back: 1

### Tareas
- T42 Interfaz de creacion de Proyecto
- T43 Asignar un responsable válido

## US04 Como administrador de sistema quiero dar de alta trabajadores
### Descripción
- Cuenta del trabajador
- Categoría
- Relacion de Roles:
 - Jefe de proyecto (1)
 - Analista (2)
 - Diseñador (3)
 - Analista-Programador (3)
 - Responsable equipo de pruebas (3)
 - Programador (4)
 - Probador (4)

### Puntos
- UX: -
- Design: 1
- Front: 1
- Back: 1

## Tareas
- T38 Crear interfaz de administración
- T39 Crear usuario

## US23 Como usuario quiero poder acceder a la aplicación
### Descripción
Implementación de un sistema de log in

Roles de acceso a la aplicación:
- Administrador del sistema
- Jefe de Proyecto
- Desarrollador

(los roles de JP y DEV pueden solaparse en el caso de usuarios en >1 proyecto distinto)

### Puntos
- UX: -
- Design: -
- Front: 1
- Back: 2

### Tareas
- T34 login
- T36 logout
- T37 Redirección del login a la vista de usuario normal o a la de administración.

# Sprint 3: 7 de Diciembre - 20 de Diciembre 2015
## US24 Como administrador de sistema quiero configurar la aplicación
### Descripción
Configurar el máximo de proyectos en los que puede participar un desarrollador. Por defecto será 2 proyectos por persona.

## US70 Como administrador quiero cargar la tabla de roles y de categorías en la aplicación
### Descripción
- Cargar datos iniciales:
    - Tablas de roles
    - Tablas de categorías

### Puntos
- UX: -
- Design: -
- Front: 2
- Back: 2

### Tareas
- T78 Interfaz y formularios
- T79 Endpoint de roles

# Sprint 4: 21 de Diciembre 2015 - 3 de Enero 2016
## US16 Como trabajador quiero rellenar mi calendario personal
### Descripción
La planificación del jefe de proyecto se hará de acuerdo con las restricciones fijadas en el calendario personal (no le puede asignar actividades en épocas de vacaciones):

- Un empleado tiene derecho a 4 semanas de vacaciones que podrá disfrutar en un máximo de 2 periodos de semanas completas.
- Un empleado puede elegir sus fechas de vacaciones en el momento que quiera, pero no puede elegirlas en periodos en los que ya tenga actividades asignadas. Una vez fijado un periodo de vacaciones no se podrá modificar.

### Puntos
- UX: 2
- Design: -
- Front: 2
- Back: 2

### Tareas
- T60 Crear la tabla Vacaciones en la BDD
- T61 Incluir las vacaciones en el modelo de usuario
- T62 Implementar la lógica para añadir vacaciones con un POST contra /holidays
- T63 [NA] Integrar fullcalendar.js en el proyecto
- T89 Evitar solapamiento con actividades
- T91 Evitar asignar actividades a un trabajador si está en vacaciones

## US09 Como jefe de proyecto quiero gestionar el inicio de mis proyectos
### Descripción
- Asignar personas al proyecto y su porcentaje de participación (Default: 100% ; nunca mayor que este valor)
- Cargar un plan de proyecto (upload)

### Puntos
- UX: 1
- Design: -
- Front: 2
- Back: 2

### Tareas
- T47 Edición de personas en un proyecto y porcentajes de participación
- T48 Cargar y descargar planes de proyecto (documentos pdf)
- T50 Determinar calendarización del proyecto (US73)
- T53 Endpoint de proyectos
- T72 Vista de proyecto inicial para configurar el proyecto

### Comentarios
La T50 debido a su complejidad y amplitud se saco a un user story propia.

# Sprint 5: 5 de Enero 2016 - 13 de Enero 2016
## US73 Como Jefe de Proyecto, quiero determinar la calendarización de las actividades de mis proyectos
### Descripción
Incluir:

- Relación de actividades; cada una de ellas con:
    - Duración estimada
    - Actividades precedentes
    - Rol asociado
- Calendarización
- Asignación de personas (plural) a actividad  (en función del rol de la persona y de la actividad)

### Puntos
- UX: 2
- Design: 3
- Front: 2
- Back: 5

### Tareas
- T74 Visualizar actividades
- T75 Determinar calendarización de cada actividad
- T76 Asignar personas a actividad según roles
- T77 Endpoint de Actividades

### Comentarios
La lógica interna de esta user story resulto ser más costosa de lo que pensamos pese a ya haber previsto su dificultad. Esto se sumo a problemas con la gestión de versiones cuando se integró la rama de esta US en la rama de la US09. Los problemas que observamos fueron la necesidad de rehacer la interfaz de usuario para adaptarla al estilo de la interfaz de la US09, la complejidad de las inserciones en la base de datos debido a el ORM bookshelf que demostró una gran dificultad de uso tanto directo como indirecto en cuanto a la gestión de las promesas de javascript y la gran cantidad de datos a validar del usuario en el cliente. La realización de esta US causó un retraso respecto al sprint que nos obligó a finalizarla en un sprint no planeado inicialmente.

## US83 Como trabajador, quiero disponer de una vista en detalle de mis proyectos
### Puntos
- UX: -
- Design: -
- Front: 3
- Back: 3

### Tareas
- T84 Definir la vista de detalle de Proyecto
- T85 Permitir la descarga del plan de Proyecto
- T87 Crear vista de detalle de la actividad

## US13 Como desarrollador quiero ver el detalle e introducr los datos de mis tareas personales
### Descripción
Introducirá los datos correspondientes a las “tareas personales” que lleva a cabo en el marco de las actividades que tiene asignadas en el proyecto o proyectos en los que está involucrado en cada periodo de tiempo (semana)

- En cada proyecto, la suma semanal del tiempo de sus actividades no deberá superar a la correspondiente a su porcentaje de particpación en el mismo (40%)
- La suma de todas las actividades de todos los proyectos en los que esté implicado no deberá superar las 40h semanales
- El sistema marcará para cada actividad, el tiempo sobreasignado

### Puntos
- UX: -
- Design: -
- Front: 5
- Back: 2

### Tareas
- T102 Modal con el detalle de la tarea y formulario para poder cerrarla e indicar el tiempo que has tardado.

## US96 Adaptar la creación de actividades para crear hitos
### Descripción
Añadir un checkbox o similar en el formulario de creación de actividades para señalar que una determinada es un hito.

### Puntos
- UX: -
- Design: -
- Front: 1
- Back: -

### Comentario
La amplitud de la user story 73 acabo por resolver esta user story.

## US33 Como jefe de proyecto quiero cerrar actividades y proyectos finalizados
### Descripción
Decidir el instante de finalización de:

- Actividades (cuando se deposite el artefacto correspondiente)
- Etapas (marcadas por un hito)
- Proyectos completos (cuando corresponda)

# Sprint 6: 14 de Enero 2016 - 22 de Enero 2016
## US71 Como cliente, quiero tener disponible la documentación de la aplicación
### Descripción
El cliente exige los siguientes documentos:

Al término de esta fase se entregará, al menos, la siguiente documentación:

- Documento de análisis y diseño.
- Documento de implementación.
- Documento de seguimiento del proyecto. En este documento se pueden incluir las medidas tomadas durante el desarrollo del mismo.
- Manual de usuario y manual de instalación.

Durante el desarrollo del proyecto, sería recomendable que cada grupo conside-
rara la inclusión de tantos hitos como considere convenientes (cada hito implicará la entrega de algún documento o la validación de algún aspecto de la aplicación). Todo ello deberá quedar reflejado tanto en el documento de plan de proyecto como en el de seguimiento del mismo.

Se supone que el código fuente está comentado adecuadamente.

### Puntos
- UX: 8
- Design: -
- Front: -
- Back: -

### Tareas
- T95 Documentacion de análisis y diseño (revisión)
- T97 Documento de despliegue
- T98 Documento de implementación
- T99 Manual de usuario e instalación
- T100 Enlace a todos los documentos desde la página principal de la aplicación, y a taiga y al repositorio
- T112 Documentación del código

## US06 Como jefe de proyecto quiero obtener informes mientras mi proyecto esté abierto
### Descripción
Generados dinámicamente.

La herramienta permitirá a los gestores de un proyecto en curso consultar las actividades que se están realizando y el esfuerzo (en horas/hombre) dedicado a cada una de ellas por los diferentes recursos humanos asignados a dichas actividades.

La herramienta permitirá a cada gestor de proyecto obtener informes de seguimiento temporal, a partir de los datos introducidos por los trabajadores adscritos del proyecto que lidera mientras esté en alguna fase de su desarrollo.

- Relación de trabajadores y sus actividades asignadas durante un periodo (semanal) determinado.
- Relación de trabajadores con **informes de actividad** pendientes de envío y fechas de los mismos.
- Relación de trabajadores con **informes de actividad** pendientes de aprobación y fechas de los mismos.
        - En estos dos últimos se podrá acceder a los informes concretos a partir de la relación anterior.
- Relación de actividades activas o finalizadas en una fecha concreta o en un periodo de tiempo, viendo el tiempo planificado y el tiempo realizado de cada una de ellas. Conviene recordar que es el jefe de proyecto el que determina la finalización de una actividad.
- Relación de actividades que han consumido o están consumiendo más tiempo del planificado.
- Actividades a realizar, así como los recursos asignados, durante un periodo de tiempo determinado posterior a la fecha actual.
- Relación de personas, con las actividades asignadas y los tiempos de trabajo para cada una de ellas para un periodo de tiempo posterior a la fecha actual (la semana siguiente, la quincena siguiente, etc).
- Al cerrar el proyecto se podrán obtener informes sobre cómo ha ido la realización del mismo.

### Puntos
- UX: -
- Design: -
- Front: 2
- Back: 2

## US14 Como desarrollador quiero consultar los datos de las actividades de mis proyectos actuales
### Descripción
Tanto de las actividades activas como las cerradas.

### Puntos
- UX: -
- Design: -
- Front: 3
- Back: 2

## US15 Como desarrollador quiero acceder a resúmenes de los proyectos finalizados
### Descripción
- En cualquier caso, no se accederá a datos personalizados
- No es necesario que haya participado en ellos para consultarlos.

### Puntos
- UX: 1
- Design: -
- Front: 2
- Back: 1

## US07 Como trabajador quiero tomar datos de mi trabajo en las actividades activas
#### Descripción
La herramienta permitirá tomar datos del trabajo realizado (solo se trabaja de lunes a viernes de cada semana), únicamente de actividades activas en ese periodo de tiempo considerado en el que se encuentre el proyecto y el rol de la persona que se haya identificado.

El usuario solo podrá modificar los datos que haya introducido de actividades activas dentro de un proyecto en curso, mientras dichas actividades no estén cerradas.

El número de “tareas personales” que una persona puede realizar por semana no debe superar las 24 entre todos los proyectos en los que esté trabajando durante la misma. Esto supone que no puede estar implicada en más de 4 actividades durante una semana.

Relación de tareas personales:

1. Trato con usuarios (llamadas, citas individuales, etc.)
2. Reuniones tanto externas como internas.
3. Lectura y revisión todo tipo de documentaciónA (externa o generada por el proyecto).
4. Elaboración de documentación (informes, documentos, documentación de programas).
5. Desarrollo y verificación de programas
6. Formación de usuarios y otras actividades (sin clasificar).

## US18 Como desarrollador quiero acceder a mis informes de actividad
### Descripción
Informe de sus actividades por semanas durante un periodo de tiempo determinado (lo puede elegir sobre un calendario), con un indicador si los informes de actividad están aceptados, rechazados o pendientes de aprobación por el correspondiente responsable de proyecto, de cualquiera de los proyectos activos en los que esté implicado. Por supuesto no se puede pedir un informe de actividad de una fecha anterior al comienzo del proyecto o posterior a la actual. Tampoco se pueden pedir informes de actividades que aún no han comenzado.

### Puntos
- UX: -
- Design: -
- Front: 1
- Back: 1

### Tareas
- Guardar los informes en el servidor

## US08 [Nth] Como jefe de proyecto quiero utilizar datos de proyectos previos para planificar y presupuestar proyectos
### Descripción
Permitir que los gestores de proyectos utilicen los datos temporales de proyectos finalizados (cerrados) para ayudar a planificar y presupuestar los nuevos y para analizar las prácticas de trabajo seguidas con el fin de servir de ayuda en cualquier proceso de mejora del proceso de desarrollo de software. Una vez finalizada una tarea existiran dos datos sobre el esfuerzo (el estimado y el real obtenido).
